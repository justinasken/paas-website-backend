from django.apps import AppConfig


class PaasConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'paas'
