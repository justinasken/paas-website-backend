from django.contrib import admin
from .models import EmailFormTemplateCategory, EmailFormType, EmailList, Email, EmailContent, InstanceStatus, LogAction, Instance, InstanceLog,ArticleTag

from rest_framework_simplejwt.token_blacklist.admin import OutstandingTokenAdmin
from rest_framework_simplejwt.token_blacklist import models


class NewOutstandingTokenAdmin(OutstandingTokenAdmin):
    def has_delete_permission(self, *args, **kwargs):
        return True  # or whatever logic you want


admin.site.unregister(models.OutstandingToken)
admin.site.register(models.OutstandingToken, NewOutstandingTokenAdmin)
admin.site.register(EmailList)
admin.site.register(ArticleTag)
admin.site.register(Email)
admin.site.register(InstanceStatus)
admin.site.register(LogAction)
admin.site.register(EmailFormTemplateCategory)
admin.site.register(EmailFormType)
admin.site.register(EmailContent)
admin.site.register(Instance)
admin.site.register(InstanceLog)