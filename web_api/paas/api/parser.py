from queue import Empty
import re
import faker
import os
import random
import string
from datetime import datetime
from django.conf import settings
from ..models import ConvertedArticle

fake = faker.Faker()
country = ""
city = ""
ip = ""

def random_faker():
    seed = int.from_bytes(os.urandom(16), 'big')
    faker.Faker(seed)
    global country 
    global city
    global ip
    country= fake.country()
    city = fake.city()
    ip = fake.ipv4()


def replace_username(email_data, text):
    username_regex = r'(?!.*EMAIL):USER-[^:]*:'
    matched = []
    for match in re.findall(username_regex, text):
        if match not in matched:
            matched.append(match)
            value = match.split("-")[1][:-1].lower()
            value = value.title()
            new_value = email_data.information.get(value)
            if new_value is not None:
                text = re.sub(match, new_value, text)
            elif email_data.information.get("Username") is not None and email_data.information.get("Username") is not False:
                new_value = email_data.information.get("Username")
                text = re.sub(match, new_value, text)
            else:
                email = email_data.email.split("@")[0]
                new_value = email
                text = re.sub(match, email, text)
    return text

def replace_emails(email_data, text):
    email_regex = r':USER-EMAIL[^:]*:'
    matched = []
    for match in re.findall(email_regex, text):
        if match not in matched:
            matched.append(match)
            value = match.split("-")[1][:-1].lower()
            new_value = ""
            if value == "emailshort":
                new_value = email_data.email.split("@")[0]
            else:
                new_value = email_data.email
            text = re.sub(match, new_value, text)
    return text

def replace_location(text):
    location_regex = r':LOCATION-RANDOM[^:]*:'
    matched = []
    for match in re.findall(location_regex, text):
        if match not in matched:
            matched.append(match)
            value = len(match.split("-"))
            new_value = ""
            if value == 3:
                new_value = country
            else:
                new_value = country+", "+city
            text = re.sub(match, new_value, text)
    return text

def replace_ip(text):
    ip_regex = r':IP-RANDOM:'
    match = re.search(ip_regex, text)
    if match is not None:
        text = re.sub(ip_regex, ip, text)
    return text

def replace_date(text):
    date_regex = r':DATE-NOW:'
    match = re.search(date_regex, text)
    if match is not None:
        now = datetime.now()
        dt_string = now.strftime("%d-%m-%Y %H:%M:%S")
        text = re.sub(date_regex, dt_string, text)
    return text

def replace_number(text):
    number_regex = r':NUMBER-RANDOM COUNT=[^:]*:'
    matched = []
    for match in re.findall(number_regex, text):
        if match not in matched:
            matched.append(match)
            value = int(match.split("=")[1][:-1])
            new_value = ''.join(random.choice(string.digits) for _ in range(value))
            text = re.sub(match, new_value, text)
    return text


def replace_link(text, instance_id, action_id, email_id):
    host = settings.CURRENT_IP
    port = settings.CURRENT_PORT
    new_url = "http://"+host+":"+port+"/api/instance/log/add/link?instance="+str(instance_id)+"&email="+str(email_id)+"&action="+str(action_id)
    link_regex = r':CUSTOM-LINK[^:]*:'
    matched = []
    for match in re.findall(link_regex, text):
        if match not in matched:
            matched.append(match)
            splited = match[len(":CUSTOM-LINK "):(len(match)-1)]
            new_value = ""
            if(splited=="URL"):
                new_value = new_url
            else:
                new_text = re.sub("TEXT=","", splited)
                new_value = "<a href="+new_url+">"+new_text+"</a>"
            text = re.sub(match, new_value, text)
    return text

def add_tracker(text, instance_id, action_id, email_id):
    host = settings.CURRENT_IP
    port = settings.CURRENT_PORT
    new_url = "http://"+host+":"+port+"/api/instance/log/add/email/"+str(instance_id)+"/"+str(email_id)+"/"+str(action_id)+"/"
    text = "<img src=\""+new_url+"\" width=\"1\" height=\"1\"/>"+text
    return text


def parse_and_process(email_data, text, instance_id, need_tracker):
    text = replace_username(email_data, text)
    text = replace_emails(email_data, text)
    text = replace_location(text)
    text = replace_ip(text)
    text = replace_date(text)
    text = replace_number(text)
    text = replace_link(text, instance_id=instance_id, action_id=2, email_id=email_data.id)
    if need_tracker:
        text = add_tracker(text, instance_id, 3, email_data.id)
    return text

def replace_articles(text, tag):
    article_regex = r':ARTICLE-[^:]*:'
    matched = []
    articles = {}
    used_articles = []
    title = ""
    for match in re.findall(article_regex, str(text)):
        if match not in matched:
            matched.append(match)
            value = match.split("-")[1][:-1].lower()
            id = value.split("id=")[1]
            value = value.split("id=")[0]
            if id not in articles:
                new_article = list(ConvertedArticle.objects.filter(tags__contains="{"+tag+"}").exclude(id__in=used_articles).all())
                new = random.choice(new_article)
                used_articles.append(new.id)
                if not articles:
                    title = new.title
                articles[id]={}
                articles[id]["title"]=new.title
                articles[id]["generated"]=new.generated
            new_value = ""
            if value == "title":
                new_value = articles[id]["title"]
            else:
                new_value = articles[id]["generated"]
            text = re.sub(match, new_value, text)
    return text, title

def replace_plain(text):
    plain_regex = r':PLAIN-RANDOM [^:]*:'
    matched = []
    for match in re.findall(plain_regex, str(text)):
        if match not in matched:
            matched.append(match)
            value = match.split(" ")[1][:-1].lower()
            new_value = ""
            if value == "greeting":
                new_value = random.choice(
                    ["Hello, :USER-USERNAME:",
                    "Greetings, :USER-USERNAME:",
                    "Good morning, :USER-USERNAME:",
                    "Good afternoon, :USER-USERNAME:",
                    "Good evening, :USER-USERNAME:",
                    "Dear :USER-USERNAME:",
                    "Hi, :USER-USERNAME:",
                    "Hey, :USER-USERNAME:!",
                    "Yo :USER-USERNAME:!",
                    "Sup :USER-USERNAME:",
                    "Heyyy :USER-USERNAME:!",
                    "Hiya :USER-USERNAME:",
                    "Howdy :USER-USERNAME:!"
                    ]
                )
            elif value == "intro":
                new_value = random.choice(
                    ["Did you know that, ",
                    "Have you heared that, ",
                    "Did you notice that, ",
                    "Here are some brief news: ",
                    "Did you hear that, ",
                    "Here are some quick facts: ",
                    "Here's quick information: ",
                    "Highlight of the day: "
                    ]
                )
            elif value == "outro":
                new_value = random.choice(
                    ["to find out more click ",
                    "for more news you can find us ",
                    "for more exciting facts click ",
                    "for more context click ",
                    "find out more about this "
                    ]
                )
                new_value+=":CUSTOM-LINK TEXT=here:."
            else:
                new_value = random.choice(
                    ["Have a great day!",
                    "Have a nice day!",
                    "Have a good one!",
                    "Have a lovely day!",
                    "Have a wonderful day!",
                    "Cheers",
                    "Cheerio",
                    ]
                )
            text = re.sub(match, new_value, text)
    return text