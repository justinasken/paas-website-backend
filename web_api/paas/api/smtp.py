from .parser import  parse_and_process
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from django.conf import settings


def sendgrid_sendmessage(receiver, sender, message, instance_id, testing):
    parsed_text = parse_and_process(receiver, message.text, instance_id, True)
    parsed_title = parse_and_process(receiver, message.title, instance_id, False)
    message = Mail(
        from_email = sender,
        to_emails = receiver.email,
        subject = parsed_title,
        html_content = parsed_text
    )
    response = None
    try:
        if testing == -1:
            api_key = settings.SENDGRID_API_KEY
            sg = SendGridAPIClient(api_key)
            response = sg.send(message)
        elif testing == 0:
            return "testing"
        else:
            raise Exception
    except Exception as e:
        response=None
    return response