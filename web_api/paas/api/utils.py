import email
from math import floor
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User
import re
import validators
from datetime import datetime
from .parser import random_faker
from django.conf import settings
from ..models import Email, EmailFormType, EmailList, Instance, LogAction, EmailContent, InstanceLog, InstanceStatus
from .smtp import sendgrid_sendmessage
form_preload_list = []

def validateEmail(email):
    response_data = {}
    response_data["response"]=""
    response_data["result"]=True
    try:
        validate_email(email)
        if len(email)>254:
            response_data["result"]=False

            response_data["response"]="email address cannot be longer than 254 characters"
    except ValidationError:
        response_data["result"]=False
        response_data["response"]="wrong email format"
    return response_data

def validatePassword(password):
    response_data = {}
    response_data["response"]="success"
    response_data["result"]=True
    if len(password) < 8:
        response_data["response"]="password is less than 8 letters"
        response_data["result"]=False
    elif re.search('[0-9]',password) is None:
        response_data["response"]="password has no number in it"
        response_data["result"]=False
    elif re.search('[A-Z]',password) is None: 
        response_data["response"]="password has no capital letter in it"
        response_data["result"]=False
    elif len(password) > 128:
        response_data["response"]="password is more than 128 letters"
        response_data["result"]=False
    return response_data

def validateUsername(username, id):
    response_data = {}
    response_data["response"]="success"
    response_data["result"]=True
    if len(username) < 6:
        response_data["response"]="username is less than 6 letters"
        response_data["result"]=False
    elif len(username) > 150:
        response_data["response"]="username is more than 150 letters"
        response_data["result"]=False
    if User.objects.exclude(pk=id).filter(username=username).exists():
        response_data["response"]="username already exists"
        response_data["result"]=False
    return response_data

def validateName(name):
    response_data = {}
    response_data["response"]="success"
    response_data["result"]=True
    if len(name.replace(" ",""))<1:
        response_data["response"]="name needs to contain letters"
        response_data["result"]=False
    if not all(x.isalpha() or x.isspace() for x in name):
        response_data["response"]="name can only contain letters"
        response_data["result"]=False
    if len(name)>150:
        response_data["response"]="name cannot be longer than 150 characters"
        response_data["result"]=False
    return response_data

def validateInstanceName(name):
    response_data = {}
    response_data["response"]="success"
    response_data["result"]=True
    if len(name.replace(" ",""))<1:
        response_data["response"]="name needs to contain more than spaces"
        response_data["result"]=False
    if len(name)>255:
        response_data["response"]="name cannot be longer than 255 characters"
        response_data["result"]=False
    return response_data

def validateListName(name):
    response_data = {}
    response_data["response"]="success"
    response_data["result"]=True
    if len(name.replace(" ",""))<1:
        response_data["response"]="name needs to contain letters or numbers"
        response_data["result"]=False
    if not all(x.isalpha() or x.isspace() or x.isnumeric() for x in name):
        response_data["response"]="name can only contain letters spaces and numbers"
        response_data["result"]=False
    if len(name)>255:
        response_data["response"]="name cannot be longer than 255 characters"
        response_data["result"]=False
    return response_data

def validateAllFields(username, password, email, first_name, last_name, id):
    user_val = validateUsername(username=username, id=id)
    pass_val = validatePassword(password=password)
    email_val = validateEmail(email=email)
    fname_val = validateName(name=first_name)
    lname_val = validateName(name=last_name)
    if user_val["result"] == False:
        return user_val
    if pass_val["result"] == False:
        return pass_val
    if email_val["result"] == False:
        return email_val
    if fname_val["result"] == False:
        return fname_val
    if lname_val["result"] == False:
        return lname_val
    return user_val

def validateInformationField(information):
    if not form_preload_list:
        forms = EmailFormType.objects.all()
        for form in forms:
            form_preload_list.append(form.type)
    response_data= {}
    response_data["result"]=True
    response_data['response']=""
    for info in information:
        if info not in form_preload_list:
            response_data['result']=False
            response_data['response']="wrong information field "+info
    return response_data


def get_user_instance_by_id(id):
    user = User.objects.get(id=id)
    return user


def create_or_update_single_email(user, email, email_list, information, email_id):
    if email_id == None:
        email_id = -1
    response_data = {}
    if email is None or email_list is None or information is None:
        response_data["result"] = "failed"
        response_data["response"] = "need to provide email, email_list id and information"
    else:
        email_val = validateEmail(email=email)
        info_val = validateInformationField(information)
        if email_val["result"] and info_val["result"]:
            try:
                el = EmailList.objects.get(
                    id=email_list, owner=get_user_instance_by_id(user.id))
                if el.used_in == 0:
                    email_check = el.email_set.filter(email=email)
                    if len(email_check)==0:
                        if email_id == -1:
                            Email.objects.create(email=email, email_list=el,
                                                    information=information)
                        else:
                            Email.objects.filter(id=email_id).update(
                                email=email, email_list=el,
                                information=information
                            )
                        response_data["result"] = "success"
                    else:
                        response_data["result"] = "failed"
                        response_data["response"]="email list already contains such email"
                else:
                    response_data["result"] = "failed"
                    response_data["response"]="email list already is used in an instance"
            except:
                response_data["result"] = "failed"
                response_data["response"] = "wrong id or your are not the owner of the list, or information is not in JSON format"
        else:
            response_data["result"] = "failed"
            response_data["response"] = email_val["response"] + \
                " "+info_val["response"]
    status = 200 if response_data['result'] == "success" else 400
    return response_data, status

def create_instance_base(user, name, list_id, title, text, description, url):
    response_data = {}
    validation_results = validateInstanceName(name)
    instance_id = None
    if validation_results["result"] and validators.url(url):
        try:
            el = EmailList.objects.get(owner = get_user_instance_by_id(user.id), id = list_id)
            instance_status = InstanceStatus.objects.get(id = 1)
            email = EmailContent.objects.create(owner = get_user_instance_by_id(user.id), text=text, title = title)
            instance = Instance.objects.create(name = name, owner = get_user_instance_by_id(user.id), email_list = el, status = instance_status, email = email, description = description, url=url)
            instance_id = instance.id
            response_data["result"] = "success"
            el.used_in +=1
            el.save()
        except Exception as e:
            response_data["result"] = "failed"
            response_data["response"] = e
    else:
        response_data["result"] = "failed"
        response_data["response"] = validation_results["response"]
    status = 200 if response_data['result'] == "success" else 400
    return response_data, status, instance_id

def send_messages_base(user, instance_id, testing=-1):
    status = 200
    response_data = {}
    try:
        instance = Instance.objects.get(id = instance_id, owner=get_user_instance_by_id(user.id))
        instance_status = instance.status
        if instance_status.id == 1:
            random_faker()
            instance_email = instance.email
            email_list = instance.email_list
            instance.status = InstanceStatus.objects.get(id=4)
            instance.save()
            new_status = 3
            emails = email_list.email_set.all()
            send_action = LogAction.objects.get(id = 1)
            sender = "fotosesija@outlook.com"
            response_data["result"]=[]
            for email in emails:
                email_response = sendgrid_sendmessage(email, sender, instance_email, instance_id, testing) 
                email_status = "success"
                if email_response is None:
                    email_status="failed"
                    new_status = 2
                else:
                    InstanceLog.objects.create(instance=instance, action = send_action, email = email)
                response_data["result"].append({
                    "email":email.email,
                    "status":email_status
                })
            instance.status = InstanceStatus.objects.get(id=new_status)
            instance.save()
        else:
            status = 400
            response_data["result"]="failed"
            response_data["response"]="already sent messages with this instance"
    except Exception as e:
        response_data["result"]="failed"
        response_data["response"]=str(e)
        status = 400
    return response_data, status

def num_to_month_short(num):
    datetime_object = datetime.strptime(num, "%m")
    month_name = datetime_object.strftime("%b")
    return month_name

def any_num_to_month_from(from_month, num):
    month = (from_month+num)%12
    if month == 0:
        month = 12
    return month

def any_num_to_year_from(from_month, from_year, num):
    new_num = floor((from_month+num)/12)
    if(new_num !=0):
        test = (from_month+num)/(12*new_num)
        if test == 1:
            new_num-=1
    year = from_year+new_num
    return year


def statistics_table(id, type, user_id):
    status = 200
    indices = {}
    logs = []
    results = []
    try:
        if type =="instance":
            instance = Instance.objects.get(id=id, owner = get_user_instance_by_id(user_id))
            email_list = instance.email_list
            emails = email_list.email_set.all()
            logs = InstanceLog.objects.filter(email__in=[email for email in emails], instance = instance).all()
            indices = {email.email:count for count, email in enumerate(emails)}
            results = [{
                "id":count,
                "email":email.email,
                "sent":False,
                "sent_date":"",
                "last_opened_email":"",
                "opened_email":False,
                "opened_email_times":0,
                "last_opened_link":"",
                "opened_link":False,
                "opened_link_times":0
            } for count, email in enumerate(emails)]
        else:
            if type == "email_list":
                email_list = EmailList.objects.get(id=id, owner = get_user_instance_by_id(user_id))
                instances = email_list.instance_set.all()
                logs = InstanceLog.objects.filter(instance__in=[instance for instance in instances]).all()
            else:
                emails = Email.objects.filter(email=id)
                instances = []
                for e in emails:
                    email_list = e.email_list
                    if email_list.owner == get_user_instance_by_id(user_id):
                        instances += email_list.instance_set.all()
                logs = InstanceLog.objects.filter(instance__in=[instance for instance in instances], email__in = [email for email in emails]).all()
            indices = {instance.id:count for count, instance in enumerate(instances)}
            results = [{
                "id":count,
                "instance_name":instance.name,
                "sent":False,
                "sent_date":"",
                "last_opened_email":"",
                "opened_email":False,
                "opened_email_times":0,
                "last_opened_link":"",
                "opened_link":False,
                "opened_link_times":0
            } for count, instance in enumerate(instances)]
        for log in logs:
            action = log.action
            if type == "instance":
                index = log.email.email
            else:
                index = log.instance.id
            if action.id == 1:
                results[indices[index]]["sent"]=True
                results[indices[index]]["sent_date"]=log.created_at.isoformat()
            elif action.id == 2:
                results[indices[index]]["opened_link"]=True
                results[indices[index]]["last_opened_link"]=log.created_at.isoformat()
                results[indices[index]]["opened_link_times"] += 1
            else:
                results[indices[index]]["opened_email"]=True
                results[indices[index]]["last_opened_email"]=log.created_at.isoformat()
                results[indices[index]]["opened_email_times"] += 1
    except:
        status = 400
    return status, results

def statistics_sent(id, type, user_id):
    status = 200
    results = {}
    total = 0
    try:
        if type == "instance":
            instance = Instance.objects.get(id=id, owner = get_user_instance_by_id(user_id))
            email_list = instance.email_list
            emails = email_list.email_set.all()
            total = len(emails)
            logs = InstanceLog.objects.filter(email__in=[email for email in emails], instance = instance, action=LogAction.objects.get(id=1)).all()
        else:
            if type == "email_list":
                email_list = EmailList.objects.get(id=id, owner = get_user_instance_by_id(user_id))
                emails = email_list.email_set.all()
                instances = email_list.instance_set.all()
                total = len(emails)*len(instances)
                logs = InstanceLog.objects.filter(instance__in=[instance for instance in instances], action=LogAction.objects.get(id=1)).all()
            else:
                emails = Email.objects.filter(email=id)
                instances = []
                for e in emails:
                    email_list = e.email_list
                    if email_list.owner == get_user_instance_by_id(user_id):
                        instances += email_list.instance_set.all()
                logs = InstanceLog.objects.filter(instance__in=[instance for instance in instances], email__in = [email for email in emails], action=LogAction.objects.get(id=1)).all()
                total = len(instances)
        results = {
            "sent":len(logs),
            "total":total
        }
    except:
        status = 400
    return status, results

def statistics_opened(id, type, user_id):
    status = 200
    results = {}
    try:
        if type == "instance":
            instance = Instance.objects.get(id=id, owner = get_user_instance_by_id(user_id))
            email_list = instance.email_list
            emails = email_list.email_set.all()
            logs = InstanceLog.objects.filter(email__in=[email for email in emails], instance = instance, action__in=[LogAction.objects.get(id=2), LogAction.objects.get(id=3)]).all()
        else:
            if type == "email_list":
                email_list = EmailList.objects.get(id=id, owner = get_user_instance_by_id(user_id))
                instances = email_list.instance_set.all()
                logs = InstanceLog.objects.filter(instance__in=[instance for instance in instances], action__in=[LogAction.objects.get(id=2), LogAction.objects.get(id=3)]).all()
            else:
                emails = Email.objects.filter(email=id)
                instances = []
                for e in emails:
                    email_list = e.email_list
                    if email_list.owner == get_user_instance_by_id(user_id):
                        instances += email_list.instance_set.all()
                logs = InstanceLog.objects.filter(instance__in=[instance for instance in instances], email__in = [email for email in emails], action__in=[LogAction.objects.get(id=2), LogAction.objects.get(id=3)]).all()
        today = datetime.now()
        results = {
            "emails":0,
            "emails_this_month":0,
            "links":0,
            "links_this_month":0
        }
        for log in logs:
            if log.action.id == 2:
                results["links"]+=1
                if log.created_at.year == today.year and log.created_at.month == today.month:
                    results["links_this_month"]+=1
            elif log.action.id == 3:
                results["emails"]+=1
                if log.created_at.year == today.year and log.created_at.month == today.month:
                    results["emails_this_month"]+=1
    except:
        status = 400
    return status, results

def statistics_months(id, type, user_id):
    status = 200
    results = []
    try:
        if type == "instance":
            instance = Instance.objects.get(id=id, owner = get_user_instance_by_id(user_id))
            email_list = instance.email_list
            emails = email_list.email_set.all()
            from_year = instance.created_at.year
            from_month = instance.created_at.month
            logs = InstanceLog.objects.filter(email__in=[email for email in emails], instance = instance).all()
        else:
            if type == "email_list":
                email_list = EmailList.objects.get(id=id, owner = get_user_instance_by_id(user_id))
                from_year = email_list.created_at.year
                from_month = email_list.created_at.month
                instances = email_list.instance_set.all()
                logs = InstanceLog.objects.filter(instance__in=[instance for instance in instances]).all()
            else:
                emails = Email.objects.filter(email=id)
                instances = []
                date = datetime.now()
                for e in emails:
                    email_list = e.email_list
                    if email_list.owner == get_user_instance_by_id(user_id):
                        new_date = datetime(e.created_at.year, e.created_at.month, e.created_at.day)
                        if new_date < date:
                            date = new_date
                        
                        instances += email_list.instance_set.all()
                logs = InstanceLog.objects.filter(instance__in=[instance for instance in instances], email__in = [email for email in emails]).all()
                from_year = date.year
                from_month = date.month
        today = datetime.now()
        to_year = today.year
        to_month = today.month
        months = (to_year-from_year)*12 + to_month-from_month
        results_index = {(any_num_to_year_from(from_month, from_year, num), any_num_to_month_from(from_month, num)):num for num in range(months+1)} 
        results = [{
            "month":num_to_month_short(str(any_num_to_month_from(from_month, num))),
            "year": any_num_to_year_from(from_month, from_year, num),
            "links":0,
            "emails":0
        } for num in range(months+1)]
        for log in logs:
            if log.action.id == 2:
                results[results_index[(log.created_at.year, log.created_at.month)]]["links"]+=1
            elif log.action.id == 3:
                results[results_index[(log.created_at.year, log.created_at.month)]]["emails"]+=1
    except:
        status = 400
    return status, results