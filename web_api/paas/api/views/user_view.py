from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.models import User
from django.http import HttpResponse
from paas.api.views import token_view
from django.contrib.auth.hashers import make_password
import json

from ..utils import validateAllFields


@api_view(['POST'])
def create_new_user(request):
    username = request.data.get('username', None)
    password = request.data.get('password', None)
    email = request.data.get('email', None)
    first_name = request.data.get('first_name', None)
    last_name = request.data.get('last_name', None)
    response_data = {}
    validation_results = validateAllFields(username, password, email, first_name, last_name,request.user.id)
    if validation_results["result"]:
        try:
            user = User.objects.create_user(
                username=username, password=password, email=email, first_name=first_name, last_name=last_name)
            token = token_view.MyTokenObtainPairSerializer.get_token(user)
            response_data['result'] = 'success'
            response_data['refresh'] = str(token)
            response_data['access'] = str(token.access_token)
        except:
            response_data['result'] = 'failed'
    else:
        response_data['result'] = 'failed'
        response_data['response'] = validation_results["response"]
    status = 200 if response_data['result']=="success" else 400
    return Response(response_data, status =status)

@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def update_user_data(request):
    username = request.data.get('username', request.user.username)
    password = request.data.get('password', request.user.password)
    confirm_password = request.data.get('confirm_password', None)
    email = request.data.get('email', request.user.email)
    first_name = request.data.get('first_name', request.user.first_name)
    last_name = request.data.get('last_name', request.user.last_name)
    response_data = {}
    if(password == confirm_password):
        validation_results = validateAllFields(username, password, email, first_name, last_name,request.user.id)
        if validation_results["result"]:
            try:
                user = User.objects.filter(pk=request.user.id)
                encoded_pass = make_password(password=password)
                if(username == request.user.username):
                    user.update(
                        email=email,
                        password=encoded_pass,
                        first_name=first_name,
                        last_name=last_name
                    )
                else:
                    user.update(
                        username=username,
                        password=encoded_pass,
                        email=email,
                        first_name=first_name,
                        last_name=last_name
                    )
                response_data['result'] = 'success'
            except:
                response_data['result'] = 'failed'
        else:
            response_data['result'] = 'failed'
            response_data['response'] = validation_results["response"]
    else:
        response_data['result'] = 'failed'
        response_data['response'] = 'password missmatch'
    status = 200 if response_data['result']=="success" else 400
    return Response(response_data, status=status)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_user_data(request):
    response_data = {
        "username":request.user.username,
        "email":request.user.email,
        "first_name":request.user.first_name,
        "last_name":request.user.last_name,
    }
    return Response(response_data)
