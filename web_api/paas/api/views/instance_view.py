from PIL import Image
from urllib.parse import urlparse
from django.http import HttpResponseRedirect, FileResponse, HttpResponse
from django.shortcuts import redirect, render
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
import validators


from ..parser import replace_articles, replace_plain

from ..serializers import ArticleTagSerializer, EmailFormTemplateCategorySerializer, InstanceEmailSerializer, InstanceSerializer, InstanceStatusSerializer
from ..utils import  get_user_instance_by_id
from ...models import ArticleTag, Email, EmailFormTemplateCategory, EmailList, Instance, LogAction, InstanceLog, InstanceStatus
from ..utils import send_messages_base, create_instance_base

@api_view(['GET'])
def instance_log_email(request, instance, email, action):
    response_data = {}
    status = 200
    action_id = action
    instance_id = instance
    email_id = email
    if status == 200 and action_id == 3:
        try:
            action = LogAction.objects.get(id = action_id)
            email = Email.objects.get(id = email_id)
            instance = Instance.objects.get(id = instance_id)
            if email.email_list.id == instance.email_list.id:
                InstanceLog.objects.create(instance = instance, action=action, email=email)
                TRANSPARENT_1_PIXEL_GIF ="\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x80\x00\x00\xff\xff\xff\x00\x00\x00\x21\xf9\x04\x01\x00\x00\x00\x00\x2c\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02\x02\x44\x01\x00\x3b"
                return HttpResponse(TRANSPARENT_1_PIXEL_GIF, content_type='image/gif')
            else:
                raise Exception("email doesnt belong to instance")
        except:
            response_data["result"] = "failed"
            response_data["response"] = "wrong fields"
            status = 400
    else:
        response_data["result"] = "failed"
        response_data["response"] = "wrong fields"
        status = 400
    return Response(response_data, status = status)

@api_view(['GET'])
def instance_log_link(request):
    instance_id = request.GET.get('instance', None)
    email_id = request.GET.get('email', None)
    action_id = request.GET.get('action', None)
    response_data = {}
    response_data["result"] = "success"
    status = 200
    if instance_id is None or email_id is None or action_id is None:
        response_data["result"] = "failed"
        response_data["response"] = "missing fields"
        status = 400
    if status == 200 and action_id=="2":
        try:
            action = LogAction.objects.get(id = action_id)
            email = Email.objects.get(id = email_id)
            instance = Instance.objects.get(id = instance_id)
            url = instance.url
            if email.email_list.id == instance.email_list.id:
                InstanceLog.objects.create(instance = instance, action=action, email=email)
                return HttpResponseRedirect(url)
            else:
                raise Exception("email or link doesnt belong to instance")
        except:
            response_data["result"] = "failed"
            response_data["response"] = "wrong fields"
            status = 400
    else:
        response_data["result"] = "failed"
        response_data["response"] = "wrong fields"
        status = 400
    return Response(response_data, status = status)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_instances(request):
    user = request.user
    instance_list = user.instance_set.all()
    serializer = InstanceSerializer(instance_list, many=True)
    return Response(serializer.data)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_instance(request):
    user = request.user
    id = request.GET.get('instance', None)
    instance = Instance.objects.get(owner = get_user_instance_by_id(user.id), id = id)
    instance_serializer = InstanceSerializer(instance)
    return Response(instance_serializer.data)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_email(request):
    instance_id = request.GET.get('instance', None)
    response_data = {}
    status = 200
    try:
        instance = Instance.objects.get(owner=get_user_instance_by_id(request.user.id), id = instance_id)
        email = instance.email
        serializer = InstanceEmailSerializer(email)
        response_data = serializer.data
    except:
        response_data["result"] = "failed"
        response_data["response"] = "not the owner of that instance"
        status = 400
    return Response(response_data, status = status)

@api_view(['GET'])
def get_email_form_template_categories(request):
    categories = EmailFormTemplateCategory.objects.all()
    serializer = EmailFormTemplateCategorySerializer(categories, many=True)
    return Response(serializer.data)

@api_view(['GET'])
def get_instance_statuses(request):
    status_list = InstanceStatus.objects.all()
    serializer = InstanceStatusSerializer(status_list, many=True)
    return Response(serializer.data)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def create_instance(request):
    name = request.data.get('name', None)
    email_list_id = request.data.get('list_id', None)
    title = request.data.get('title', None)
    data = request.data.get('text', None)
    description = request.data.get('description', None)
    url = request.data.get('url', None)
    response_data, status, instance_id = create_instance_base(request.user, name, email_list_id, title, data, description, url)
    return Response(response_data, status=status)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def send_messages(request):
    instance_id = request.data.get('instance_id', None)
    testing = request.data.get("testing",-1)
    response_data = {}
    status = 200
    response_data, status = send_messages_base(request.user, instance_id,testing)
    return Response(response_data, status)

@api_view(['POST'])
@permission_classes([IsAuthenticated])
def create_and_send_messages(request):
    name = request.data.get('name', None)
    email_list_id = request.data.get('list_id', None)
    title = request.data.get('title', None)
    data = request.data.get('text', None)
    description = request.data.get('description', None)
    url = request.data.get('url', None)
    response_data, status, instance_id = create_instance_base(request.user, name, email_list_id, title, data, description, url)
    if status == 200:
        response_data, status = send_messages_base(request.user, instance_id)
    return Response(response_data, status)

@api_view(["PUT"])
@permission_classes([IsAuthenticated])
def update_instance(request):
    id = request.data.get('id', None)
    name = request.data.get('name', None)
    email_list_id = request.data.get('list_id', None)
    title = request.data.get('title', None)
    data = request.data.get('text', None)
    description = request.data.get('description', None)
    url = request.data.get('url', None)
    response_data = {}
    status = 200
    try:
        instance = Instance.objects.get(id=id, owner = get_user_instance_by_id(request.user.id))       
        if instance.status.id == 1:
            if(email_list_id != None):
                email_list = EmailList.objects.get(id=email_list_id, owner = get_user_instance_by_id(request.user.id))
                el = instance.email_list
                el.used_in -= 1
                el.save()
                email_list.used_in += 1
                email_list.save()
                instance.email_list = email_list
            email = instance.email
            if(data != None):
                email.text=data
            if(title != None):
                email.title=title
            if(name != None):
                instance.name=name
            if(description!=None):
                instance.description=description
            if(url!=None and validators.url(url)):
                instance.url=url
            email.save()
            instance.save()
            response_data["result"] = "success"
            status = 200
        else:
            response_data["result"] = "failed"
            response_data["response"] = "instance has already been launched"
            status = 400
    except:
        response_data["result"] = "failed"
        response_data["response"] = "not the owner of that instance"
        status = 400
    return Response(response_data, status)

@api_view(["DELETE"])
@permission_classes([IsAuthenticated])
def remove_instance(request):
    id = request.data.get('id', None)
    response_data = {}
    if id is None:
        response_data["result"] = "failed"
        response_data["response"] = "need to provide id"
    else:
        try:
            instance = Instance.objects.get(
                id=id, owner=get_user_instance_by_id(request.user.id))
            if instance.status.id != 4:
                el = instance.email_list
                el.used_in -= 1
                el.save()
                instance.delete()
                response_data["result"] = "success"
            else:
                response_data["result"] = "failed"
                response_data["response"] = "instance sending messages in progress"
        except:
            response_data["result"] = "failed"
            response_data["response"] = "wrong id or your are not the owner of the instance"
    status = 200 if response_data['result'] == "success" else 400
    return Response(response_data, status=status)

@api_view(["DELETE"])
@permission_classes([IsAuthenticated])
def remove_multiple_instances(request):
    id_list = dict(request.data).get("ids",[])
    response_data = {}
    if id_list is None or not id_list:
        response_data["result"] = "failed"
        response_data["response"] = "need to provide list of ids"
    else:
        try:
            objects_to_delete = Instance.objects.filter(
                pk__in=[int(id) for id in id_list], owner=get_user_instance_by_id(request.user.id)).exclude(status=InstanceStatus.objects.get(id=4))
            test_obj_count = Instance.objects.filter(pk__in=[int(id) for id in id_list], owner=get_user_instance_by_id(request.user.id), status=InstanceStatus.objects.get(id=4)).count()
            if test_obj_count>0:    
                raise Exception
            for obj in objects_to_delete:
                el = obj.email_list
                el.used_in -= 1
                el.save()
            objects_to_delete.delete()
            response_data["result"] = "success"
        except:
            response_data["result"] = "failed"
            response_data["response"] = "wrong id, instance is in progress, or your are not the owner of the instance"
    status = 200 if response_data['result'] == "success" else 400
    return Response(response_data, status=status)

@api_view(["GET"])
@permission_classes([IsAuthenticated])
def get_article_tags(request):
    tags = ArticleTag.objects.all()
    return Response(ArticleTagSerializer(tags, many=True).data)

@api_view(["POST"])
@permission_classes([IsAuthenticated])
def get_converted_article(request):
    text = request.data.get("text","")
    tag = request.data.get("tag","sport")
    text, title = replace_articles(text, tag)
    text = replace_plain(text)
    result = {
        "text":text,
        "title":title
    }
    return Response(result)