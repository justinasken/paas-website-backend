from urllib import response
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
import json
from ..utils import create_or_update_single_email, get_user_instance_by_id, validateEmail, validateInformationField, validateListName
from ..serializers import EmailFormTypeSerializer, EmailListSerializer, EmailSerializer
from paas.models import EmailFormType, EmailList, Email
from django.contrib.auth.models import User


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_email_lists(request):
    user = request.user
    email_lists = user.emaillist_set.all()
    serializer = EmailListSerializer(email_lists, many=True)
    return Response(serializer.data)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def create_new_list(request):
    name = request.data.get('name', None)
    response_data = {}
    try:
        validation_results = validateListName(name)
        if validation_results["result"]:
            el = EmailList(
                name=name, owner=get_user_instance_by_id(request.user.id), used_in = 0)
            el.save()
            response_data["result"] = "success"
        else:
            response_data["result"] = "failed"
            response_data["response"] = validation_results["response"]
    except:
        response_data["result"] = "failed"
        response_data["response"]="no data"
    status = 200 if response_data['result'] == "success" else 400
    return Response(response_data, status=status)


@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def remove_email_list(request):
    id = request.data.get('id', None)
    response_data = {}
    if id is None:
        response_data["result"] = "failed"
        response_data["response"] = "need to provide id"
    else:
        try:
            el = EmailList.objects.get(
                id=id, owner=get_user_instance_by_id(request.user.id))
            el.delete()
            response_data["result"] = "success"
        except:
            response_data["result"] = "failed"
            response_data["response"] = "wrong id or your are not the owner of the list"
    status = 200 if response_data['result'] == "success" else 400
    return Response(response_data, status=status)


@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def update_email_list(request):
    email_id = request.data.get('id', None)
    name = request.data.get('name', None)
    response_data = {}
    try:
        validation_results = validateListName(name)
        if validation_results["result"] and email_id is not None:
            try:
                el = EmailList.objects.get(
                    id=email_id, owner=get_user_instance_by_id(request.user.id))
                el.name=name
                el.save()
                response_data["result"] = "success"
            except:
                response_data["result"] = "failed"
                response_data["response"] = "wrong id or your are not the owner of the list"
        else:
            response_data["result"] = "failed"
            response_data["response"] = "need to provide id and name"
    except:
        response_data["result"] = "failed"
        response_data["response"] = "need to provide id and name"
    status = 200 if response_data['result'] == "success" else 400
    return Response(response_data, status=status)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_email_list(request):
    user = request.user
    list_id = request.GET.get('list_id', None)
    try:
        email_list = EmailList.objects.get(
            id=list_id, owner=get_user_instance_by_id(user.id))
        serializer = EmailListSerializer(email_list)
        data = serializer.data
        return Response(data)
    except:
        return Response({"result": "failed"}, status=400)


@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def remove_multiple_email_list(request):
    id_list = dict(request.data).get("ids",[])
    response_data = {}
    if id_list is None or not id_list:
        response_data["result"] = "failed"
        response_data["response"] = "need to provide list of ids"
    else:
        try:
            objects_to_delete = EmailList.objects.filter(
                pk__in=[int(id) for id in id_list], owner=get_user_instance_by_id(request.user.id))
            test_count = EmailList.objects.filter(
                pk__in=[int(id) for id in id_list]).count()
            if test_count == objects_to_delete.count():
                objects_to_delete.delete()
                response_data["result"] = "success"
            else:
                raise Exception
        except:
            response_data["result"] = "failed"
            response_data["response"] = "wrong id or your are not the owner of the list"
    status = 200 if response_data['result'] == "success" else 400
    return Response(response_data, status=status)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_emails_from_list(request):
    user = request.user
    list_id = request.GET.get('list_id', None)
    try:
        email_list = EmailList.objects.get(
            id=list_id, owner=get_user_instance_by_id(user.id))
        emails = email_list.email_set.all()
        serializer = EmailSerializer(emails, many=True)
        data = serializer.data
        return Response(data)
    except:
        return Response({"result": "failed"}, status=400)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def create_new_email(request):
    email = request.data.get('email', None)
    email_list = request.data.get('list_id', None)
    information = request.data.get('information', None)
    response_data, status = create_or_update_single_email(
        request.user, email, email_list, information, None)
    return Response(response_data, status=status)


@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def remove_email(request):
    email_id = request.data.get('id', None)
    response_data = {}
    if email_id is None:
        response_data["result"] = "failed"
        response_data["response"] = "need to provide id"
    else:
        try:
            e = Email.objects.get(id=email_id)
            el = EmailList.objects.get(
                id=e.email_list.id, owner=get_user_instance_by_id(request.user.id))
            if el.used_in == 0:
                e.delete()
                response_data["result"] = "success"
            else:
                response_data["result"] = "failed"
                response_data["response"] = "email list is used in another instance"
        except:
            response_data["result"] = "failed"
            response_data["response"] = "wrong id or your are not the owner of the list"
    status = 200 if response_data['result'] == "success" else 400
    return Response(response_data, status=status)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_email_from_list(request):
    user = request.user
    list_id = request.GET.get('list_id', None)
    email_id = request.GET.get('email_id', None)
    try:
        email_list = EmailList.objects.get(
            id=list_id, owner=get_user_instance_by_id(user.id))
        email = Email.objects.get(id=email_id)
        serializer = EmailSerializer(email)
        data = serializer.data
        return Response(data)
    except:
        return Response({"result": "failed"}, status=400)


@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def update_email(request):
    email_id = request.data.get('id', None)
    email = request.data.get('email', None)
    email_list = request.data.get('list_id', None)
    information = request.data.get('information', None)
    response_data = {}
    status=200
    if email_id is None or email is None or email_list is None or information is None:
        response_data["result"] = "failed"
        response_data["response"] = "feelds cannot be empty"
        status=400
    else:
        rd, status = create_or_update_single_email(
            request.user, email, email_list, information, email_id)
        response_data = rd
    return Response(response_data, status=status)


@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
def remove_multiple_email(request):
    id_list = dict(request.data).get("ids",[])
    list_id = request.data.get("list_id", None)
    response_data = {}
    if id_list is None or not id_list or list_id is None:
        response_data["result"] = "failed"
        response_data["response"] = "need to provide list of ids and email list id"
    else:
        try:
            email_list = EmailList.objects.get(
                id=list_id, owner=get_user_instance_by_id(request.user.id))
            test_list = EmailList.objects.get(id=list_id)
            if email_list==test_list:
                if email_list.used_in == 0:
                    emails = email_list.email_set.all()
                    objects_to_delete = emails.filter(pk__in=[id for id in id_list])
                    objects_to_delete.delete()
                    response_data["result"] = "success"
                else:
                    response_data["result"] = "failed"
                    response_data["response"] = "email list is used in another instance"
            else:
                raise Exception
        except:
            response_data["result"] = "failed"
            response_data["response"] = "wrong id or your are not the owner of the list"
    status = 200 if response_data['result'] == "success" else 400
    return Response(response_data, status=status)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def create_multiple_new_email(request):
    emails = request.data.get("emails",None)
    list_id = request.data.get('list_id', None)
    response_data = {}
    s = 200
    if not emails or emails is None or list_id is None:
        response_data["result"] = "failed"
        response_data["response"] = "need to provide list of emails and email list id"
        s = 400
    else:
        for e in emails:
            email = e["email"]
            information = e["information"]
            rd, status = create_or_update_single_email(
                request.user, email, list_id, information, None)
            if status != 200:
                s = status
            response_data[email] = rd
    return Response(response_data, status=s)


@api_view(["GET"])
def get_emails_form_list(request):
    forms = EmailFormType.objects.all()
    serializer = EmailFormTypeSerializer(forms, many=True)
    return Response(serializer.data)

@api_view(["GET"])
@permission_classes([IsAuthenticated])
def get_all_emails(request):
    user = request.user
    email_lists = user.emaillist_set.all()
    emails = []
    emails_class = []
    for el in email_lists:
        e = el.email_set.all()
        for email in e:
            if email.email not in emails:
                emails.append(email.email)
                emails_class.append(email)
    serializer = EmailSerializer(emails_class, many=True)
    return Response(serializer.data)