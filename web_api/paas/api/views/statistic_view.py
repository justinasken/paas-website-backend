from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from ..utils import statistics_table, statistics_sent, statistics_opened, statistics_months

@api_view(["GET"])
@permission_classes([IsAuthenticated])
def get_statistics_table(request, type):
    id = request.GET.get('id', None)
    status, results = statistics_table(id, type, request.user.id)
    if status != 200:
        results = "not the owner of the instance"
    return Response(results, status)

@api_view(["GET"])
@permission_classes([IsAuthenticated])
def get_statistics_sent(request, type):
    id = request.GET.get('id', None)
    status, results = statistics_sent(id, type, request.user.id)
    if status != 200:
        results = "not the owner of the instance"
    return Response(results, status)

@api_view(["GET"])
@permission_classes([IsAuthenticated])
def get_statistics_opened(request, type):
    id = request.GET.get('id', None)
    status, results = statistics_opened(id, type, request.user.id)
    if status != 200:
        results = "not the owner of the instance"
    return Response(results, status)

@api_view(["GET"])
@permission_classes([IsAuthenticated])
def get_statistics_months(request, type):
    id = request.GET.get('id', None)
    status, results = statistics_months(id, type, request.user.id)
    if status != 200:
        results = "not the owner of the instance"
    return Response(results, status)

