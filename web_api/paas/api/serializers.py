from dataclasses import field
from rest_framework import serializers
from paas.models import Email, EmailFormTemplateCategory, EmailFormType, EmailList, Instance, EmailContent, InstanceStatus, ArticleTag, ConvertedArticle


class EmailListSerializer(serializers.ModelSerializer):
    email_count = serializers.SerializerMethodField('_count_emails')

    def _count_emails(self, obj):
        count = obj.email_set.count()
        return count

    class Meta:
        model = EmailList
        fields = ['id', 'name', 'owner', 'created_at', 'email_count', 'used_in']


class EmailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Email
        fields = '__all__'

class EmailFormTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmailFormType
        fields = '__all__'

class InstanceSerializer(serializers.ModelSerializer):
    email_list_name = serializers.SerializerMethodField('_get_email_list_name')
    email_text = serializers.SerializerMethodField('_get_email_text')
    email_title = serializers.SerializerMethodField('_get_email_title')
    status_name = serializers.SerializerMethodField('_get_state_name')
    def _get_email_list_name(self, obj):
        el = obj.email_list
        return el.name
    def _get_email_text(self, obj):
        e = obj.email
        return e.text
    def _get_email_title(self, obj):
        e = obj.email
        return e.title
    def _get_state_name(self, obj):
        state = obj.status
        return state.status

    class Meta:
        model = Instance
        fields = ['id', 'name', 'owner', 'email_list','email_list_name','status','status_name', 'email', 'email_text', 'email_title', 'description', 'url', 'created_at']
        

class InstanceStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = InstanceStatus
        fields = "__all__"

class InstanceEmailSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmailContent
        fields = "__all__"
        
class EmailFormTemplateCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = EmailFormTemplateCategory
        fields = "__all__"

class ArticleTagSerializer(serializers.ModelSerializer):
    article_count = serializers.SerializerMethodField('_count_articles')
    def _count_articles(self, obj):
        count = ConvertedArticle.objects.filter(tags__contains="{"+obj.tag+"}").count()
        return count
    class Meta:
        model = ArticleTag
        fields = ["id","tag","article_count"]