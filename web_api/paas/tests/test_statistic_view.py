from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from django.contrib.auth.models import User
from ..models import *

class TestAppStatisticView(TestCase):
    def setUp(self):
        idle = InstanceStatus.objects.create(status="IDLE")
        finished = InstanceStatus.objects.create(status="FINISHED")
        error = InstanceStatus.objects.create(status="ERROR")
        in_progress = InstanceStatus.objects.create(status="IN PROGRESS")
        self.sent = LogAction.objects.create(id=1,action="EMAIL SENT")
        self.opened_link = LogAction.objects.create(id=2,action="OPEN LINK")
        self.opened_email = LogAction.objects.create(id=3,action="OPEN EMAIL")
        self.user_valid=User.objects.create(username="testusername",password="testpassword")
        self.user_invalid = User.objects.create(username="invalidusername",password="invalidpassword")
        self.email_list_valid = EmailList.objects.create(name="test email list 0", owner=self.user_valid, used_in=0)
        self.email_list_invalid = EmailList.objects.create(name="invalid test email list", owner=self.user_invalid, used_in=0)
        self.instance_email_valid = EmailContent.objects.create(text="test",title="title",owner=self.user_valid)
        self.instance_email_invalid = EmailContent.objects.create(text="invalid test", title="invalid title",owner=self.user_invalid)
        self.instance_valid=Instance.objects.create(name="test instance",owner=self.user_valid, email_list=self.email_list_valid,status=finished,email=self.instance_email_valid,description="",url="")
        self.instance_invalid = Instance.objects.create(name="invalid test instance",owner=self.user_invalid,email_list=self.email_list_invalid, status=finished, email=self.instance_email_invalid,description="",url="")
        self.email_valid_0 = Email.objects.create(email="test@test.com", email_list=self.email_list_valid, information="")
        self.email_valid_1 = Email.objects.create(email="testa@test.com", email_list=self.email_list_valid, information="")
        self.email_invalid_0 = Email.objects.create(email="testb@test.com", email_list=self.email_list_invalid, information="")
        self.email_invalid_1 = Email.objects.create(email="test@test.com", email_list=self.email_list_invalid, information="")
        InstanceLog.objects.create(instance=self.instance_valid,email=self.email_valid_0, action=self.sent)
        InstanceLog.objects.create(instance=self.instance_valid,email=self.email_valid_0, action=self.opened_email)
        InstanceLog.objects.create(instance=self.instance_valid,email=self.email_valid_0, action=self.opened_link)
        InstanceLog.objects.create(instance=self.instance_valid,email=self.email_valid_1, action=self.sent)
        InstanceLog.objects.create(instance=self.instance_valid,email=self.email_valid_1, action=self.opened_email)
        InstanceLog.objects.create(instance=self.instance_valid,email=self.email_valid_1, action=self.opened_link)
        InstanceLog.objects.create(instance=self.instance_invalid,email=self.email_invalid_0, action=self.sent)
        InstanceLog.objects.create(instance=self.instance_invalid,email=self.email_invalid_0, action=self.opened_email)
        InstanceLog.objects.create(instance=self.instance_invalid,email=self.email_invalid_0, action=self.opened_link)
        InstanceLog.objects.create(instance=self.instance_invalid,email=self.email_invalid_1, action=self.sent)
        InstanceLog.objects.create(instance=self.instance_invalid,email=self.email_invalid_1, action=self.opened_email)
        InstanceLog.objects.create(instance=self.instance_invalid,email=self.email_invalid_1, action=self.opened_link)
        self.client = APIClient()
        self.client.force_authenticate(user=self.user_valid)
        self.invalid_client = APIClient()
        self.invalid_client.force_authenticate(user=self.user_invalid)


    def test_get_statistics_table_valid(self):
        url_instance = "%s?id=%s"%(reverse('get-statistics-table', kwargs={"type":"instance"}),self.instance_valid.id)
        url_email_list = "%s?id=%s"%(reverse('get-statistics-table', kwargs={"type":"email_list"}),self.email_list_valid.id)
        url_email = "%s?id=%s"%(reverse('get-statistics-table', kwargs={"type":"email"}),self.email_valid_0.id)
        response_instance = self.client.get(url_instance)
        response_email_list = self.client.get(url_email_list)
        response_email = self.client.get(url_email)
        self.assertEqual(response_instance.status_code, 200)
        self.assertEqual(response_email_list.status_code, 200)
        self.assertEqual(response_email.status_code, 200)

    def test_get_statistics_table_invalid(self):
        url_instance = "%s?id=%s"%(reverse('get-statistics-table', kwargs={"type":"instance"}),self.instance_valid.id)
        url_email_list = "%s?id=%s"%(reverse('get-statistics-table', kwargs={"type":"email_list"}),self.email_list_valid.id)
        response_instance = self.invalid_client.get(url_instance)
        response_email_list = self.invalid_client.get(url_email_list)
        self.assertEqual(response_instance.status_code, 400)
        self.assertEqual(response_email_list.status_code, 400)
    
    def test_get_statistics_sent_valid(self):
        url_instance = "%s?id=%s"%(reverse('get-statistics-sent', kwargs={"type":"instance"}),self.instance_valid.id)
        url_email_list = "%s?id=%s"%(reverse('get-statistics-sent', kwargs={"type":"email_list"}),self.email_list_valid.id)
        url_email = "%s?id=%s"%(reverse('get-statistics-sent', kwargs={"type":"email"}),self.email_valid_0.id)
        response_instance = self.client.get(url_instance)
        response_email_list = self.client.get(url_email_list)
        response_email = self.client.get(url_email)
        self.assertEqual(response_instance.status_code, 200)
        self.assertEqual(response_email_list.status_code, 200)
        self.assertEqual(response_email.status_code, 200)

    def test_get_statistics_sent_invalid(self):
        url_instance = "%s?id=%s"%(reverse('get-statistics-sent', kwargs={"type":"instance"}),self.instance_valid.id)
        url_email_list = "%s?id=%s"%(reverse('get-statistics-sent', kwargs={"type":"email_list"}),self.email_list_valid.id)
        response_instance = self.invalid_client.get(url_instance)
        response_email_list = self.invalid_client.get(url_email_list)
        self.assertEqual(response_instance.status_code, 400)
        self.assertEqual(response_email_list.status_code, 400)

    def test_get_statistics_opened_valid(self):
        url_instance = "%s?id=%s"%(reverse('get-statistics-opened', kwargs={"type":"instance"}),self.instance_valid.id)
        url_email_list = "%s?id=%s"%(reverse('get-statistics-opened', kwargs={"type":"email_list"}),self.email_list_valid.id)
        url_email = "%s?id=%s"%(reverse('get-statistics-opened', kwargs={"type":"email"}),self.email_valid_0.id)
        response_instance = self.client.get(url_instance)
        response_email_list = self.client.get(url_email_list)
        response_email = self.client.get(url_email)
        self.assertEqual(response_instance.status_code, 200)
        self.assertEqual(response_email_list.status_code, 200)
        self.assertEqual(response_email.status_code, 200)

    def test_get_statistics_opened_invalid(self):
        url_instance = "%s?id=%s"%(reverse('get-statistics-opened', kwargs={"type":"instance"}),self.instance_valid.id)
        url_email_list = "%s?id=%s"%(reverse('get-statistics-opened', kwargs={"type":"email_list"}),self.email_list_valid.id)
        response_instance = self.invalid_client.get(url_instance)
        response_email_list = self.invalid_client.get(url_email_list)
        self.assertEqual(response_instance.status_code, 400)
        self.assertEqual(response_email_list.status_code, 400)

    def test_get_statistics_months_valid(self):
        url_instance = "%s?id=%s"%(reverse('get-statistics-months', kwargs={"type":"instance"}),self.instance_valid.id)
        url_email_list = "%s?id=%s"%(reverse('get-statistics-months', kwargs={"type":"email_list"}),self.email_list_valid.id)
        url_email = "%s?id=%s"%(reverse('get-statistics-months', kwargs={"type":"email"}),self.email_valid_0.id)
        response_instance = self.client.get(url_instance)
        response_email_list = self.client.get(url_email_list)
        response_email = self.client.get(url_email)
        self.assertEqual(response_instance.status_code, 200)
        self.assertEqual(response_email_list.status_code, 200)
        self.assertEqual(response_email.status_code, 200)

    def test_get_statistics_months_invalid(self):
        url_instance = "%s?id=%s"%(reverse('get-statistics-months', kwargs={"type":"instance"}),self.instance_valid.id)
        url_email_list = "%s?id=%s"%(reverse('get-statistics-months', kwargs={"type":"email_list"}),self.email_list_valid.id)
        response_instance = self.invalid_client.get(url_instance)
        response_email_list = self.invalid_client.get(url_email_list)
        self.assertEqual(response_instance.status_code, 400)
        self.assertEqual(response_email_list.status_code, 400)

