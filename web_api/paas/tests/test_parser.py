from django.test import TestCase
from ..api.parser import *
from ..models import Email, EmailList
from django.contrib.auth.models import User

class TestAppParser(TestCase):
    def setUp(self):
        self.user=User.objects.create(username="u",password="p")
        email_list = EmailList.objects.create(name="test",owner=self.user,used_in=0)
        self.email_with_all = Email.objects.create(email="firstemail@email.com",email_list=email_list, information={
                "Username":"first_username",
                "Custom":"custom_username"
            })
        self.email_without_custom = Email.objects.create(email="secondemail@email.com",email_list=email_list, information={
                "Username":"second_username",
            })
        self.email_without_any = Email.objects.create(email="thirdemail@email.com",email_list=email_list, information={})
        random_faker()

    def test_replace_username(self):
        user_username_text = "test :USER-USERNAME:, testas: :USER-USERNAME: end USER-USERNAME"
        user_custom_text = "test :USER-CUSTOM:, testas: :USER-CUSTOM: end USER-CUSTOM"
        self.assertEqual(replace_username(self.email_with_all, user_username_text), "test first_username, testas: first_username end USER-USERNAME")
        self.assertEqual(replace_username(self.email_with_all, user_custom_text), "test custom_username, testas: custom_username end USER-CUSTOM")
        self.assertEqual(replace_username(self.email_without_custom, user_custom_text), "test second_username, testas: second_username end USER-CUSTOM")
        self.assertEqual(replace_username(self.email_without_any, user_custom_text), "test thirdemail, testas: thirdemail end USER-CUSTOM")

    def test_replace_email(self):
        email_text = "test :USER-EMAIL:, testas: :USER-EMAIL: end USER-EMAIL"
        email_short_text = "test :USER-EMAILSHORT:, testas: :USER-EMAILSHORT: end USER-EMAILSHORT"
        self.assertEqual(replace_emails(self.email_with_all, email_text), "test firstemail@email.com, testas: firstemail@email.com end USER-EMAIL")
        self.assertEqual(replace_emails(self.email_without_custom, email_short_text), "test secondemail, testas: secondemail end USER-EMAILSHORT")
    
    def test_replace_location(self):
        pattern = r':LOCATION-RANDOM[^:]*:'
        pattern_none = r'LOCATION-RANDOM[^:]*'
        location_text_country = "test :LOCATION-RANDOM-COUNTRY: LOCATION-RANDOM-COUNTRY test"
        location_text_full = "test :LOCATION-RANDOM: LOCATION-RANDOM test"
        location_text_none = "test LOCATION-RANDOM LOCATION-RANDOM-COUNTRY test"
        self.assertNotRegex(replace_location(location_text_country), pattern)
        self.assertNotRegex(replace_location(location_text_full), pattern)
        self.assertRegex(replace_location(location_text_none), pattern_none)

    def test_replace_ip(self):
        pattern = r':IP-RANDOM:'
        pattern_none = r'IP-RANDOM'
        ip_text = "test :IP-RANDOM: IP-RANDOM test"
        ip_text_none = "test IP-RANDOM test"
        self.assertNotRegex(replace_ip(ip_text), pattern)
        self.assertRegex(replace_ip(ip_text_none), pattern_none)
    
    def test_replace_date(self):
        pattern = r':DATE-NOW:'
        pattern_none = r'DATE-NOW'
        date_text = "test :DATE-NOW: DATE-NOW"
        date_text_none = "test DATE-NOW test"
        self.assertNotRegex(replace_date(date_text), pattern)
        self.assertRegex(replace_date(date_text_none), pattern_none)

    def test_replace_number(self):
        pattern = r':NUMBER-RANDOM COUNT=[^:]*:'
        pattern_none = r'NUMBER-RANDOM COUNT=[^:]*'
        number_text = "test :NUMBER-RANDOM COUNT=6: test"
        number_text_none = "test NUMBER-RANDOM COUNT=6 test"
        self.assertNotRegex(replace_number(number_text), pattern)
        self.assertRegex(replace_number(number_text_none), pattern_none)

    def test_replace_number(self):
        pattern = r':NUMBER-RANDOM COUNT=[^:]*:'
        pattern_none = r'NUMBER-RANDOM COUNT=[^:]*'
        number_text = "test :NUMBER-RANDOM COUNT=6: test"
        number_text_none = "test NUMBER-RANDOM COUNT=6 test"
        self.assertNotRegex(replace_number(number_text), pattern)
        self.assertRegex(replace_number(number_text_none), pattern_none)

    def test_replace_link(self):
        host = settings.CURRENT_IP
        port = settings.CURRENT_PORT
        new_url = "http://"+host+":"+port+"/api/instance/log/add/link?instance=0&email=0&action=0"
        url_text_pure = "test :CUSTOM-LINK URL: CUSTOM-LINK URL test"
        url_text_html = "test :CUSTOM-LINK TEXT=test_test: CUSTOM-LINK TEXT=test_test test"
        self.assertEqual(replace_link(url_text_pure, 0, 0, 0), "test "+new_url+" CUSTOM-LINK URL test")
        self.assertEqual(replace_link(url_text_html, 0, 0, 0), "test <a href="+new_url+">test_test</a> CUSTOM-LINK TEXT=test_test test")

    def test_add_tracker(self):
        host = settings.CURRENT_IP
        port = settings.CURRENT_PORT
        new_url = "http://"+host+":"+port+"/api/instance/log/add/email/0/0/0/"
        starting_text = "test_text"
        text = "<img src=\""+new_url+"\" width=\"1\" height=\"1\"/>"+starting_text
        self.assertEqual(add_tracker(starting_text,0,0,0),text)

    def test_replace_articles(self):
        ConvertedArticle.objects.create(url="www.google.com", tags="{sport}", title="test_title0",generated="test_generated0")
        ConvertedArticle.objects.create(url="www.google.net", tags="{sport}", title="test_title1",generated="test_generated1")
        tag = "sport"
        pattern = r':ARTICLE-[^:]*:'
        pattern_none = r'ARTICLE-[^:]*'
        articles_text_single="test :ARTICLE-GENERATED ID=0: :ARTICLE-TITLE ID=0: test"
        articles_text_single_none="test ARTICLE-GENERATED ID=0 ARTICLE-TITLE ID=0 test"
        articles_text_multiple="test :ARTICLE-GENERATED ID=0: :ARTICLE-TITLE ID=0: :ARTICLE-GENERATED ID=1: :ARTICLE-TITLE ID=1: test"
        articles_text_multiple_none="test ARTICLE-GENERATED ID=0 ARTICLE-TITLE ID=0 ARTICLE-GENERATED ID=1 ARTICLE-TITLE ID=1 test"
        self.assertNotRegex(replace_articles(articles_text_single, tag)[0], pattern)
        self.assertNotRegex(replace_articles(articles_text_multiple, tag)[0], pattern)
        self.assertRegex(replace_articles(articles_text_single_none, tag)[0], pattern_none)
        self.assertRegex(replace_articles(articles_text_multiple_none, tag)[0], pattern_none)

    def test_replace_plain(self):
        pattern = r':PLAIN-RANDOM [^:]*:'
        pattern_none = r'PLAIN-RANDOM [^:]*'
        plain_text = "test :PLAIN-RANDOM GREETING: :PLAIN-RANDOM INTRO: :PLAIN-RANDOM-OUTRO: :PLAIN-RANDOM GOODBYE: test"
        plain_text_none = "test PLAIN-RANDOM GREETING PLAIN-RANDOM INTRO PLAIN-RANDOM-OUTRO PLAIN-RANDOM GOODBYE test"
        self.assertNotRegex(replace_plain(plain_text), pattern)
        self.assertRegex(replace_plain(plain_text_none), pattern_none)