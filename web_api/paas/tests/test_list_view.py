from urllib import response
from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from django.contrib.auth.models import User
import json
import ast
from ..models import *

class TestAppListView(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create(username="user",password="pass")
        self.client.force_authenticate(user=self.user)
        self.sent = LogAction.objects.create(id=1,action="EMAIL SENT")
        self.opened_link = LogAction.objects.create(id=2,action="OPEN LINK")
        self.opened_email = LogAction.objects.create(id=3,action="OPEN EMAIL")
        self.idle = InstanceStatus.objects.create(id=1,status="IDLE")
        self.finished = InstanceStatus.objects.create(id=2,status="FINISHED")
        self.error = InstanceStatus.objects.create(id=3,status="ERROR")
        self.in_progress = InstanceStatus.objects.create(id=4,status="IN PROGRESS")
        self.email_list = EmailList.objects.create(name="email_list",owner=self.user,used_in=0)
        self.instance_email = EmailContent.objects.create(text="text",title="title",owner=self.user)
        self.email = Email.objects.create(email="test@test.com",email_list=self.email_list,information={})
        self.instance = Instance.objects.create(name="instance",owner=self.user,email_list=self.email_list, status=self.idle, email=self.instance_email, description="",url="http://www.google.com")
        category = EmailFormTemplateCategory.objects.create(category="test category")
        EmailFormType.objects.create(type="test",has_username=False, template="",title="",category=category)
    
    def test_create_multiple_new_email_invalid(self):
        url = reverse("create-multiple-emails")
        data = {
            "list_id":self.email_list.id,
            "emails":[
                        {
                    "email":"test",
                    "information":{"test":"testasd"}
                },
                {
                    "email":"testxzc@asd.com",
                    "information":{"test":"testasd"}
                }
            ]
        }
        response = self.client.post(url,data=data,format="json")
        response_no_data = self.client.post(url)
        self.assertEqual(response.status_code,400)
        self.assertEqual(response_no_data.status_code,400)
    
    def test_create_multiple_new_email_valid(self):
        url = reverse("create-multiple-emails")
        data = {
            "list_id":self.email_list.id,
            "emails":[
                        {
                    "email":"test@testasv.com",
                    "information":{"test":"testasd"}
                },
                {
                    "email":"testxzc@asd.com",
                    "information":{"test":"testasd"}
                }
            ]
        }
        response = self.client.post(url,data=data,format="json")
        self.assertEqual(response.status_code,200)

    def test_remove_multiple_email_invalid(self):
        new_user = User.objects.create(username="u",password="p")
        email_list = EmailList.objects.create(name="email_list",owner=new_user,used_in=1)
        email0 = Email.objects.create(email="testnewb@test.com",email_list=email_list,information={})
        email1 = Email.objects.create(email="testnewa@test.com",email_list=email_list,information={})
        url = reverse("remove-selected-emails")
        data = {
            "ids":[email0.id,email1.id],
            "list_id":email_list.id
        }
        response = self.client.delete(url,data=data)
        response_no_data = self.client.delete(url)
        self.assertEqual(response.status_code,400)
        self.assertEqual(response_no_data.status_code,400)
    
    def test_remove_multiple_email_valid(self):
        email0 = Email.objects.create(email="testnewb@test.com",email_list=self.email_list,information={})
        email1 = Email.objects.create(email="testnewa@test.com",email_list=self.email_list,information={})
        url = reverse("remove-selected-emails")
        data = {
            "ids":[email0.id,email1.id],
            "list_id":self.email_list.id
        }
        response = self.client.delete(url,data=data)
        self.assertEqual(response.status_code,200)
    
    def test_update_email_invalid(self):
        email = Email.objects.create(email="testnew@test.com",email_list=self.email_list,information={})
        url = reverse("update-email")
        data={
            "id":email.id,
            "email":"updatedemail",
            "list_id":self.email_list.id,
            "information":{"test":"testasd"}
        }
        response = self.client.put(url, data=data,format='json')
        response_no_data = self.client.put(url)
        updated = Email.objects.get(id=email.id)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response_no_data.status_code, 400)
        self.assertEqual(updated.email, "testnew@test.com")

    def test_update_email_valid(self):
        email = Email.objects.create(email="testnew@test.com",email_list=self.email_list,information={})
        url = reverse("update-email")
        data={
            "id":email.id,
            "email":"updatedemail@email.com",
            "list_id":self.email_list.id,
            "information":{"test":"testasd"}
        }
        response = self.client.put(url, data=data,format='json')
        updated = Email.objects.get(id=email.id)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(updated.email, "updatedemail@email.com")
    
    def test_get_email_from_list_invalid(self):
        url = "%s"%(reverse("get-email-from-email-list"))
        response = self.client.get(url)
        self.assertEqual(response.status_code,400)
    
    def test_get_email_from_list_valid(self):
        url = "%s?list_id=%s&email_id=%s"%(reverse("get-email-from-email-list"),self.email_list.id, self.email.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code,200)

    def test_get_emails_form_list(self):
        url = reverse("get-all-email-forms")
        response = self.client.get(url)
        self.assertEqual(response.status_code,200)
    
    def test_get_all_emails(self):
        url = reverse("get-all-emails-from-user")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_remove_email_invalid(self):
        url = reverse("remove-email")
        data = {
            "id":-1
        }
        response = self.client.delete(url, data=data)
        response_no_data = self.client.delete(url)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response_no_data.status_code, 400)
    
    def test_remove_email_valid(self):
        email = Email.objects.create(email="test@testas.com",email_list=self.email_list,information={})
        url = reverse("remove-email")
        data = {
            "id":email.id
        }
        response = self.client.delete(url, data=data)
        try:
            removed = Email.objects.get(id=email.id)
        except:
            removed = None
        self.assertEqual(removed, None)
        self.assertEqual(response.status_code,200)

    def test_create_new_email_invalid(self):
        url = reverse("create-email")
        data = {
            "email":"test",
            "list_id":self.email_list.id,
            "information":{"test":"testasd"}
        }
        response = self.client.post(url,data=data,format='json')
        response_no_data = self.client.post(url)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response_no_data.status_code, 400)
    
    def test_create_new_email_valid(self):
        url = reverse("create-email")
        data = {
            "email":"test@test.net",
            "list_id":self.email_list.id,
            "information":{"test":"testasd"}
        }
        response = self.client.post(url,data=data,format='json')
        try:
            new = Email.objects.get(email="test@test.net")
        except:
            new = None
        self.assertNotEqual(new, None)
        self.assertEqual(response.status_code, 200)
    
    def test_get_emails_from_list_invalid(self):
        url_bad_id = "%s?list_id=%s" %(reverse("get-all-emails-from-email-list"),-1)
        url_no_data = reverse("get-all-emails-from-email-list")
        response_id = self.client.get(url_bad_id)
        response_data = self.client.get(url_no_data)
        self.assertEqual(response_id.status_code, 400)
        self.assertEqual(response_data.status_code, 400)
    
    def test_get_emails_from_list_valid(self):
        url = "%s?list_id=%s" %(reverse("get-all-emails-from-email-list"),self.email_list.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)


    def test_remove_multiple_email_list_invalid(self):
        new_user = User.objects.create(username="u",password="p")
        temp0 = EmailList.objects.create(name="temp",owner=new_user,used_in=0)
        temp1 = EmailList.objects.create(name="temp",owner=new_user,used_in=0)
        url = reverse("remove-selected-email-list")
        data = {
            "ids":[temp0.id, temp1.id]
        }
        response = self.client.delete(url, data=data)
        response_no_data = self.client.delete(url)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response_no_data.status_code, 400)
    
    def test_remove_multiple_email_list_valid(self):
        temp0 = EmailList.objects.create(name="temp",owner=self.user,used_in=0)
        temp1 = EmailList.objects.create(name="temp",owner=self.user,used_in=0)
        url = reverse("remove-selected-email-list")
        data = {
            "ids":[temp0.id, temp1.id]
        }
        response = self.client.delete(url, data=data)
        self.assertEqual(response.status_code, 200)

    def test_get_email_list_invalid(self):
        url=reverse("get-email-list")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 400)

    def test_get_email_list_valid(self):
        url="%s?list_id=%s"%(reverse("get-email-list"),self.email_list.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_email_list_invalid(self):
        temp = EmailList.objects.create(name="temp",owner=self.user,used_in=0)
        url=reverse("update-email-list")
        data = {
            "id":temp.id,
            "name":" "
        }
        response = self.client.put(url,data=data)
        response_no_data = self.client.put(url)
        updated = EmailList.objects.get(id=temp.id)
        self.assertEqual(updated.name, "temp")
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response_no_data.status_code, 400)

    def test_update_email_list_valid(self):
        temp = EmailList.objects.create(name="temp",owner=self.user,used_in=0)
        url=reverse("update-email-list")
        data = {
            "id":temp.id,
            "name":"updatedname"
        }
        response = self.client.put(url,data=data)
        updated = EmailList.objects.get(id=temp.id)
        self.assertEqual(updated.name, "updatedname")
        self.assertEqual(response.status_code, 200)

    def test_remove_email_list_invalid(self):
        url=reverse("remove-email-list")
        data = {
            "id":-1
        }
        response = self.client.delete(url,data=data)
        response_no_data = self.client.delete(url)
        self.assertEqual(response.status_code,400)
        self.assertEqual(response_no_data.status_code, 400)

    def test_remove_email_list_valid(self):
        temp = EmailList.objects.create(name="temp",owner=self.user,used_in=0)
        url=reverse("remove-email-list")
        data = {
            "id":temp.id
        }
        response = self.client.delete(url,data=data)
        try:
            removed = EmailList.objects.get(id=temp.id)
        except:
            removed = None
        self.assertEqual(response.status_code,200)
        self.assertEqual(removed, None)

    def test_create_new_list_invalid(self):
        url=reverse("create-email-list")
        data = {
            "name":" "
        }
        response = self.client.post(url,data=data)
        response_no_data = self.client.post(url)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response_no_data.status_code, 400)
    
    def test_create_new_list_valid(self):
        url=reverse("create-email-list")
        data = {
            "name":"unique name for email list"
        }
        response = self.client.post(url,data=data)
        try:
            new = EmailList.objects.get(name="unique name for email list")
        except:
            new=None
        self.assertNotEqual(new, None)
        self.assertEqual(response.status_code, 200)

    def test_get_email_lists(self):
        EmailList.objects.create(name="email_list_other",owner=self.user,used_in=0)
        url = reverse('get-all-email-lists')
        response = self.client.get(url)
        self.assertGreater(len(response.data),1)
        self.assertEqual(response.status_code, 200)
