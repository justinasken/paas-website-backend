from pydoc import describe
from django.test import TestCase
from django.contrib.auth.models import User
from ..models import EmailList, Email, EmailContent, InstanceStatus, LogAction, EmailFormTemplateCategory, EmailFormType, Instance, ArticleTag

class TestAppModels(TestCase):
    def setUp(self):
        self.user = User.objects.create(username="testuser",password="testuser")
        self.email_list = EmailList.objects.create(name="testemaillist", owner=self.user, used_in=0)
        self.category = EmailFormTemplateCategory.objects.create(category="testcategory")
        self.status = InstanceStatus.objects.create(status="teststatus")
        self.instance_email = EmailContent.objects.create(text="test text", title="testtitle",owner=self.user)

    def test_email_list_model_str(self):
        test_obj = str(self.user.id)+"| Test Email List"
        email_list = EmailList.objects.create(name="Test Email List", owner=self.user,used_in=0)
        self.assertEqual(str(email_list),test_obj) 
    
    def test_email_model_str(self):
        test_obj=str(self.email_list.id)+"| test@email.com"
        email = Email.objects.create(email="test@email.com", email_list=self.email_list, information="{}")
        self.assertEqual(str(email), test_obj)

    def test_instance_status_model_str(self):
        instance_status = InstanceStatus.objects.create(status="test_status")
        test_obj="test_status"
        self.assertEqual(str(instance_status),test_obj)

    def test_instance_action_model_str(self):
        instance_action = LogAction.objects.create(action="test_action")
        test_obj="test_action"
        self.assertEqual(str(instance_action), test_obj)

    def test_email_form_template_category_model_str(self):
        category = EmailFormTemplateCategory.objects.create(category="test_category")
        test_obj="test_category"
        self.assertEqual(str(category),test_obj)
    
    def test_email_form_type_model_str(self):
        type = EmailFormType.objects.create(type="TEST",has_username=False,template="",title="",category=self.category)
        test_obj="TEST"
        self.assertEqual(str(type),test_obj)

    def test_instance_model_str(self):
        instance = Instance.objects.create(name="test_instance", owner=self.user, email_list=self.email_list,status=self.status,email=self.instance_email,description="",url="")
        test_obj=str(self.user.id)+"| test_instance"
        self.assertEqual(str(instance),test_obj)

    def test_article_tag_model_str(self):
        tag = ArticleTag.objects.create(tag="test_tag")
        test_obj="test_tag"
        self.assertEqual(str(tag),test_obj)