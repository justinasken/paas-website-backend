from django.test import TestCase

from ..models import *
from ..api.utils import *
from django.contrib.auth.models import User

class TestAppUtilsValidators(TestCase):
    def setUp(self):
        self.user = User.objects.create(username="validusername", password="testpassword")
        User.objects.create(username="testusername", password="testpassword")
        User.objects.create(username="newusername", password="testpassword")
        category = EmailFormTemplateCategory.objects.create(category="test category")
        EmailFormType.objects.create(type="test",has_username=False, template="",title="",category=category)

    def test_validate_email_valid(self):
        valid_email_0 = "test@email.com"
        valid_email_1 = "test123@email.com"
        valid_email_2 = "456546@email.com"
        self.assertTrue(validateEmail(valid_email_0)["result"])
        self.assertTrue(validateEmail(valid_email_1)["result"])
        self.assertTrue(validateEmail(valid_email_2)["result"])
    
    def test_validate_email_invalid(self):
        invalid_email_0 = "test"
        invalid_email_1 = "test.com"
        invalid_email_2 = "test@email"
        invalid_email_3 = "@test"
        invalid_email_4 = "@test.com"
        invalid_email_5 = ".com"
        invalid_email_6 = "test'.]]'[-=@email.com"
        invalid_email_7 = "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890@email.com"
        self.assertFalse(validateEmail(invalid_email_0)["result"])
        self.assertFalse(validateEmail(invalid_email_1)["result"])
        self.assertFalse(validateEmail(invalid_email_2)["result"])
        self.assertFalse(validateEmail(invalid_email_3)["result"])
        self.assertFalse(validateEmail(invalid_email_4)["result"])
        self.assertFalse(validateEmail(invalid_email_5)["result"])
        self.assertFalse(validateEmail(invalid_email_6)["result"])
        self.assertFalse(validateEmail(invalid_email_7)["result"])

    def test_validate_password_valid(self):
        valid_password_0="TestPassword123"
        valid_password_1="!TestPassword!./,123456"
        self.assertTrue(validatePassword(valid_password_0)["result"])
        self.assertTrue(validatePassword(valid_password_1)["result"])

    def test_validate_password_invalid(self):
        invalid_password_less_than_8 = "test"
        invalid_password_no_numbers = "testtesttest"
        invalid_password_no_upper = "testte123456789"
        invalid_password_more_than_128 = "Testte123456789123456789123456789123456789123456789123456789123456789Testte123456789123456789123456789123456789123456789123456789123456789Testte123456789123456789123456789123456789123456789123456789123456789Testte123456789123456789123456789123456789123456789123456789123456789Testte123456789123456789123456789123456789123456789123456789123456789Testte123456789123456789123456789123456789123456789123456789123456789Testte123456789123456789123456789123456789123456789123456789123456789Testte123456789123456789123456789123456789123456789123456789123456789"
        self.assertFalse(validatePassword(invalid_password_less_than_8)["result"])
        self.assertFalse(validatePassword(invalid_password_no_numbers)["result"])
        self.assertFalse(validatePassword(invalid_password_no_upper)["result"])
        self.assertFalse(validatePassword(invalid_password_more_than_128)["result"])

    def test_validate_username_valid(self):
        valid_username_0 = "testuser"
        valid_username_1 = "validusername"
        self.assertTrue(validateUsername(valid_username_0, self.user.id)["result"])
        self.assertTrue(validateUsername(valid_username_1, self.user.id)["result"])

    def test_validate_username_invalid(self):
        invalid_username_less_than_6="test"
        invalid_username_more_than_150="Testte123456789123456789123456789123456789123456789123456789123456789Testte123456789123456789123456789123456789123456789123456789123456789Testte123456789123456789123456789123456789123456789123456789123456789Testte123456789123456789123456789123456789123456789123456789123456789Testte123456789123456789123456789123456789123456789123456789123456789Testte123456789123456789123456789123456789123456789123456789123456789Testte123456789123456789123456789123456789123456789123456789123456789Testte123456789123456789123456789123456789123456789123456789123456789"
        invalid_username_same_not_owner_0="testusername"
        invalid_username_same_not_owner_1="newusername"
        self.assertFalse(validateUsername(invalid_username_less_than_6, self.user.id)["result"])
        self.assertFalse(validateUsername(invalid_username_more_than_150, self.user.id)["result"])
        self.assertFalse(validateUsername(invalid_username_same_not_owner_0,self.user.id)["result"])
        self.assertFalse(validateUsername(invalid_username_same_not_owner_1,self.user.id)["result"])

    def test_validate_name_valid(self):
        valid_name_0="test"
        valid_name_1="test test"
        self.assertTrue(validateName(valid_name_0)["result"])
        self.assertTrue(validateName(valid_name_1)["result"])

    def test_validate_name_invalid(self):
        invalid_name_empty=""
        invalid_name_not_all_letters="test15"
        invalid_name_more_than_150="asdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasd"
        self.assertFalse(validateName(invalid_name_empty)["result"])
        self.assertFalse(validateName(invalid_name_not_all_letters)["result"])
        self.assertFalse(validateName(invalid_name_more_than_150)["result"])

    def test_validate_instance_name_valid(self):
        valid_name="test instance name"
        self.assertTrue(validateInstanceName(valid_name)["result"])

    def test_validate_instance_name_invalid(self):
        invalid_name_empty=" "
        invalid_name_more_than_254="asdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasd"
        self.assertFalse(validateInstanceName(invalid_name_empty)["result"])
        self.assertFalse(validateInstanceName(invalid_name_more_than_254)["result"])

    def test_validate_list_name_valid(self):
        valid_name_0 = "test"
        valid_name_1 = "test 154"
        valid_name_2 = "1557"
        self.assertTrue(validateListName(valid_name_0)["result"])
        self.assertTrue(validateListName(valid_name_1)["result"])
        self.assertTrue(validateListName(valid_name_2)["result"])

    def test_validate_list_name_invalid(self):
        invalid_name_empty=" "
        invalid_name_symbols="test name /?.,"
        invalid_name_more_than_254="asdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasdasd"
        self.assertFalse(validateListName(invalid_name_empty)["result"])
        self.assertFalse(validateListName(invalid_name_symbols)["result"])
        self.assertFalse(validateListName(invalid_name_more_than_254)["result"])

    def test_validate_information_field_valid(self):
        valid_field = {"test":"testasd"}
        self.assertTrue(validateInformationField(valid_field)["result"])

    def test_validate_information_field_invalid(self):
        invalid_field = {"invalid field":"test"}
        self.assertFalse(validateInformationField(invalid_field)["result"])

    def test_validate_all_fields_valid(self):
        valid_username = "Test123456789"
        valid_password = "Test123456789"
        valid_email = "test@email.com"
        valid_name = "test"
        self.assertTrue(validateAllFields(username=valid_username,password=valid_password,email=valid_email,first_name=valid_name,last_name=valid_name, id=self.user.id))

    def test_validate_all_fields_invalid(self):
        valid_username = "Test123456789"
        valid_password = "Test123456789"
        valid_email = "test@email.com"
        valid_name = "test"
        invalid_username = "testusername"
        invalid_password = "test"
        invalid_email = "test"
        invalid_name = ""
        self.assertFalse(validateAllFields(username=invalid_username,password=valid_password,email=valid_email,first_name=valid_name,last_name=valid_name,id=self.user.id)["result"])
        self.assertFalse(validateAllFields(username=valid_username,password=invalid_password,email=valid_email,first_name=valid_name,last_name=valid_name,id=self.user.id)["result"])
        self.assertFalse(validateAllFields(username=valid_username,password=valid_password,email=invalid_email,first_name=valid_name,last_name=valid_name,id=self.user.id)["result"])
        self.assertFalse(validateAllFields(username=valid_username,password=valid_password,email=valid_email,first_name=invalid_name,last_name=valid_name,id=self.user.id)["result"])
        self.assertFalse(validateAllFields(username=valid_username,password=valid_password,email=valid_email,first_name=valid_name,last_name=invalid_name,id=self.user.id)["result"])

class TestAppUtilsMisc(TestCase):
    def setUp(self):
        self.sent = LogAction.objects.create(id=1,action="EMAIL SENT")
        self.opened_link = LogAction.objects.create(id=2,action="OPEN LINK")
        self.opened_email = LogAction.objects.create(id=3,action="OPEN EMAIL")
        self.user = User.objects.create(username="testusername", password="testpassword")
        self.invalid_user = User.objects.create(username="t",password="p")
        self.idle = InstanceStatus.objects.create(id=1,status="IDLE")
        self.finished = InstanceStatus.objects.create(id=2,status="FINISHED")
        self.error = InstanceStatus.objects.create(id=3,status="ERROR")
        self.in_progress = InstanceStatus.objects.create(id=4,status="IN PROGRESS")
        self.email_list = EmailList.objects.create(name="test email list", owner=self.user, used_in=0)
        self.instance_email = EmailContent.objects.create(text="test",title="title",owner=self.user)
        Email.objects.create(email="test@test.com",email_list=self.email_list, information={})
        self.wrong_email_list = EmailList.objects.create(name="invalid email list",owner=self.user, used_in=1)

    def test_get_user_instance_by_id(self):
        self.assertEqual(get_user_instance_by_id(self.user.id),self.user)

    def test_any_num_to_month_from(self):
        self.assertEqual(any_num_to_month_from(5,13),6)
        self.assertEqual(any_num_to_month_from(5,2),7)
        self.assertEqual(any_num_to_month_from(5,28),9)
        self.assertEqual(any_num_to_month_from(1,11),12)

    def test_any_num_to_year_from(self):
        self.assertEqual(any_num_to_year_from(5, 2011, 13), 2012)
        self.assertEqual(any_num_to_year_from(5, 2011, 2), 2011)
        self.assertEqual(any_num_to_year_from(5, 2011, 25), 2013)
        self.assertEqual(any_num_to_year_from(11, 2011, 1), 2011)

    def test_num_to_month_short(self):
        self.assertEqual(num_to_month_short("1"),"Jan")
        self.assertEqual(num_to_month_short("2"),"Feb")
        self.assertEqual(num_to_month_short("3"),"Mar")
        self.assertEqual(num_to_month_short("4"),"Apr")
        self.assertEqual(num_to_month_short("5"),"May")
        self.assertEqual(num_to_month_short("6"),"Jun")
        self.assertEqual(num_to_month_short("7"),"Jul")
        self.assertEqual(num_to_month_short("8"),"Aug")
        self.assertEqual(num_to_month_short("9"),"Sep")
        self.assertEqual(num_to_month_short("10"),"Oct")
        self.assertEqual(num_to_month_short("11"),"Nov")
        self.assertEqual(num_to_month_short("12"),"Dec")
    
    def test_send_messages_base_valid(self):
        instance = Instance.objects.create(name="test",owner=self.user,email_list=self.email_list,status=self.idle,email=self.instance_email,description="",url="")
        response_data, status = send_messages_base(user=self.user, instance_id=instance.id,testing=0)
        self.assertEqual(status, 200)
    
    def test_send_messages_base_invalid(self):
        invalid_user = User.objects.create(username="u",password="p")
        instance = Instance.objects.create(name="test",owner=self.user,email_list=self.email_list,status=self.idle,email=self.instance_email,description="",url="")
        instance_finished = Instance.objects.create(name="test",owner=self.user,email_list=self.email_list,status=self.finished,email=self.instance_email,description="",url="")
        instance_error = Instance.objects.create(name="test",owner=self.user,email_list=self.email_list,status=self.error,email=self.instance_email,description="",url="")
        instance_in_progress = Instance.objects.create(name="test",owner=self.user,email_list=self.email_list,status=self.in_progress,email=self.instance_email,description="",url="")
        self.assertEqual(send_messages_base(user=self.user, instance_id=instance_finished.id,testing=0)[1], 400)
        self.assertEqual(send_messages_base(user=self.user, instance_id=instance_error.id,testing=0)[1], 400)
        self.assertEqual(send_messages_base(user=self.user, instance_id=instance_in_progress.id,testing=0)[1], 400)
        self.assertEqual(send_messages_base(user=self.user, instance_id=instance.id,testing=1)[0]["result"], [{"email":"test@test.com","status":"failed"}])
        self.assertEqual(send_messages_base(user=invalid_user, instance_id=instance_in_progress.id,testing=0)[1], 400)

    def test_create_instance_base_valid(self):
        response_data, status, instance_id = create_instance_base(user=self.user, name="test create instance", list_id=self.email_list.id, title="test",text="test",description="",url="http://www.google.com")
        self.assertEqual(status, 200)
    
    def test_create_instance_base_invalid(self):
        invalid_user = User.objects.create(username="u",password="p")
        self.assertEqual(create_instance_base(user=invalid_user, name="test create instance", list_id=self.email_list.id, title="test",text="test",description="",url="http://www.google.com")[1],400)
        self.assertEqual(create_instance_base(user=invalid_user, name="test create instance", list_id=self.email_list.id, title="test",text="test",description="",url="")[1],400)
    
    def test_create_or_update_single_email_valid(self):
        new_email = Email.objects.create(email="test@testemail.com",email_list=self.email_list, information={})
        result, status_update = create_or_update_single_email(user=self.user,email="updated@gmail.com", email_list=self.email_list.id,information={},email_id=new_email.id)
        result, status_create = create_or_update_single_email(user=self.user,email="newtest@gmail.com", email_list=self.email_list.id,information={},email_id=None)
        self.assertEqual(status_update,200)
        self.assertEqual(status_create,200)
        self.assertEqual(Email.objects.get(id=new_email.id).email,"updated@gmail.com")
        

    def test_create_or_update_single_email_invalid(self):
        result, status_none = create_or_update_single_email(user=self.user,email=None, email_list=self.email_list.id,information={},email_id=None)
        result, status_bad_email = create_or_update_single_email(user=self.user,email="test", email_list=self.email_list.id,information={},email_id=None)
        result, status_bad_info = create_or_update_single_email(user=self.user,email="test@testzxc.com", email_list=self.email_list.id,information={"wrong info":"test"},email_id=None)
        result, status_email_exists = create_or_update_single_email(user=self.user,email="test@test.com", email_list=self.email_list.id,information={},email_id=None)
        result, status_email_list_used = create_or_update_single_email(user=self.user,email="testasd@test.com", email_list=self.wrong_email_list.id,information={},email_id=None)
        result, status_email_invalid_user = create_or_update_single_email(user=self.invalid_user,email="testasd@test.com", email_list=self.email_list.id,information={},email_id=None)
        self.assertEqual(status_none,400)
        self.assertEqual(status_bad_email,400)
        self.assertEqual(status_bad_info,400)
        self.assertEqual(status_email_exists,400)
        self.assertEqual(status_email_list_used,400)
        self.assertEqual(status_email_invalid_user,400)


class TestAppUtilsStatistics(TestCase):
    def setUp(self):
        idle = InstanceStatus.objects.create(status="IDLE")
        finished = InstanceStatus.objects.create(status="FINISHED")
        error = InstanceStatus.objects.create(status="ERROR")
        in_progress = InstanceStatus.objects.create(status="IN PROGRESS")
        self.sent = LogAction.objects.create(id=1,action="EMAIL SENT")
        self.opened_link = LogAction.objects.create(id=2,action="OPEN LINK")
        self.opened_email = LogAction.objects.create(id=3,action="OPEN EMAIL")
        self.user_valid=User.objects.create(username="testusername",password="testpassword")
        self.user_invalid = User.objects.create(username="invalidusername",password="invalidpassword")
        self.email_list_valid = EmailList.objects.create(name="test email list 0", owner=self.user_valid, used_in=0)
        self.email_list_invalid = EmailList.objects.create(name="invalid test email list", owner=self.user_invalid, used_in=0)
        self.instance_email_valid = EmailContent.objects.create(text="test",title="title",owner=self.user_valid)
        self.instance_email_invalid = EmailContent.objects.create(text="invalid test", title="invalid title",owner=self.user_invalid)
        self.instance_valid=Instance.objects.create(name="test instance",owner=self.user_valid, email_list=self.email_list_valid,status=finished,email=self.instance_email_valid,description="",url="")
        self.instance_invalid = Instance.objects.create(name="invalid test instance",owner=self.user_invalid,email_list=self.email_list_invalid, status=finished, email=self.instance_email_invalid,description="",url="")
        self.email_valid_0 = Email.objects.create(email="test@test.com", email_list=self.email_list_valid, information="")
        self.email_valid_1 = Email.objects.create(email="testa@test.com", email_list=self.email_list_valid, information="")
        self.email_invalid_0 = Email.objects.create(email="testb@test.com", email_list=self.email_list_invalid, information="")
        self.email_invalid_1 = Email.objects.create(email="test@test.com", email_list=self.email_list_invalid, information="")
        InstanceLog.objects.create(instance=self.instance_valid,email=self.email_valid_0, action=self.sent)
        InstanceLog.objects.create(instance=self.instance_valid,email=self.email_valid_0, action=self.opened_email)
        InstanceLog.objects.create(instance=self.instance_valid,email=self.email_valid_0, action=self.opened_link)
        InstanceLog.objects.create(instance=self.instance_valid,email=self.email_valid_1, action=self.sent)
        InstanceLog.objects.create(instance=self.instance_valid,email=self.email_valid_1, action=self.opened_email)
        InstanceLog.objects.create(instance=self.instance_valid,email=self.email_valid_1, action=self.opened_link)
        InstanceLog.objects.create(instance=self.instance_invalid,email=self.email_invalid_0, action=self.sent)
        InstanceLog.objects.create(instance=self.instance_invalid,email=self.email_invalid_0, action=self.opened_email)
        InstanceLog.objects.create(instance=self.instance_invalid,email=self.email_invalid_0, action=self.opened_link)
        InstanceLog.objects.create(instance=self.instance_invalid,email=self.email_invalid_1, action=self.sent)
        InstanceLog.objects.create(instance=self.instance_invalid,email=self.email_invalid_1, action=self.opened_email)
        InstanceLog.objects.create(instance=self.instance_invalid,email=self.email_invalid_1, action=self.opened_link)

    def test_statistics_table_instance_valid(self):
        type = "instance"
        id = self.instance_valid.id
        user_id = self.user_valid.id
        emails = self.email_list_valid.email_set.all()
        results = [{
                "id":count,
                "email":email.email,
                "sent":True,
                "sent_date":"",
                "last_opened_email":"",
                "opened_email":True,
                "opened_email_times":1,
                "last_opened_link":"",
                "opened_link":True,
                "opened_link_times":1
            } for count, email in enumerate(emails)]
        test_status, test_results = statistics_table(id,type,user_id)
        for count,res in enumerate(results):
            res["sent_date"]=test_results[count]["sent_date"]
            res["last_opened_email"]=test_results[count]["last_opened_email"]
            res["last_opened_link"]=test_results[count]["last_opened_link"]
        self.assertEqual(test_status, 200)
        self.assertEqual(test_results, results)

    def test_statistics_table_instance_invalid(self):
        type = "instance"
        self.assertEqual(statistics_table(self.instance_valid.id,type,self.user_invalid.id)[0], 400)
        self.assertEqual(statistics_table(self.instance_invalid.id,type,self.user_valid.id)[0], 400)

    def test_statistics_table_email_list_valid(self):
        type = "email_list"
        id = self.email_list_valid.id
        user_id = self.user_valid.id
        instances = self.email_list_valid.instance_set.all()
        results = [{
                "id":count,
                "instance_name":instance.name,
                "sent":True,
                "sent_date":"",
                "last_opened_email":"",
                "opened_email":True,
                "opened_email_times":2,
                "last_opened_link":"",
                "opened_link":True,
                "opened_link_times":2
            } for count, instance in enumerate(instances)]
        test_status, test_results = statistics_table(id,type,user_id)
        for count,res in enumerate(results):
            res["sent_date"]=test_results[count]["sent_date"]
            res["last_opened_email"]=test_results[count]["last_opened_email"]
            res["last_opened_link"]=test_results[count]["last_opened_link"]
        self.assertEqual(test_status, 200)
        self.assertEqual(test_results, results)

    def test_statistics_table_email_list_invalid(self):
        type = "email_list"
        self.assertEqual(statistics_table(self.email_list_valid.id,type,self.user_invalid.id)[0], 400)
        self.assertEqual(statistics_table(self.email_list_invalid.id,type,self.user_valid.id)[0], 400)
    
    def test_statistics_table_email_valid(self):
        type = "email"
        id = self.email_valid_0.email
        user_id = self.user_valid.id
        emails = Email.objects.filter(email=id)
        instances = []
        for e in emails:
            email_list = e.email_list
            if email_list.owner == get_user_instance_by_id(user_id):
                instances += email_list.instance_set.all()
        results = [{
                "id":count,
                "instance_name":instance.name,
                "sent":True,
                "sent_date":"",
                "last_opened_email":"",
                "opened_email":True,
                "opened_email_times":1,
                "last_opened_link":"",
                "opened_link":True,
                "opened_link_times":1
            } for count, instance in enumerate(instances)]
        test_status, test_results = statistics_table(id,type,user_id)
        for count,res in enumerate(results):
            res["sent_date"]=test_results[count]["sent_date"]
            res["last_opened_email"]=test_results[count]["last_opened_email"]
            res["last_opened_link"]=test_results[count]["last_opened_link"]
        self.assertEqual(test_status, 200)
        self.assertEqual(test_results, results)

    def test_statistics_sent_instance_valid(self):
        type = "instance"
        id = self.instance_valid.id
        user_id = self.user_valid.id
        test_status, test_results = statistics_sent(id,type,user_id)
        results={'sent':2,'total':2}
        self.assertEqual(test_status, 200)
        self.assertEqual(test_results, results)

    def test_statistics_sent_instance_invalid(self):
        type = "instance"
        self.assertEqual(statistics_sent(self.instance_valid.id,type,self.user_invalid.id)[0], 400)
        self.assertEqual(statistics_sent(self.instance_invalid.id,type,self.user_valid.id)[0], 400)

    def test_statistics_sent_email_list_valid(self):
        type = "email_list"
        id = self.email_list_valid.id
        user_id = self.user_valid.id
        test_status, test_results = statistics_sent(id,type,user_id)
        results={'sent':2,'total':2}
        self.assertEqual(test_status, 200)
        self.assertEqual(test_results, results)

    def test_statistics_sent_email_list_invalid(self):
        type = "email_list"
        self.assertEqual(statistics_sent(self.email_list_valid.id,type,self.user_invalid.id)[0], 400)
        self.assertEqual(statistics_sent(self.email_list_invalid.id,type,self.user_valid.id)[0], 400)
    
    def test_statistics_sent_email_valid(self):
        type = "email"
        id = self.email_valid_0.email
        user_id = self.user_valid.id
        test_status, test_results = statistics_sent(id,type,user_id)
        results={'sent':1,'total':1}
        self.assertEqual(test_status, 200)
        self.assertEqual(test_results, results)

    def test_statistics_sent_email_invalid(self):
        type = "email"
        id = "invalid"
        user_id = self.user_valid.id
        test_status, test_results = statistics_sent(id,type,user_id)
        results={'sent':0,'total':0}
        self.assertEqual(test_results, results)

    def test_statistics_opened_instance_valid(self):
        type = "instance"
        id = self.instance_valid.id
        user_id = self.user_valid.id
        results = {
            "emails":2,
            "emails_this_month":2,
            "links":2,
            "links_this_month":2
        }
        test_status, test_results = statistics_opened(id,type,user_id)
        self.assertEqual(test_status, 200)
        self.assertEqual(test_results, results)

    def test_statistics_opened_instance_invalid(self):
        type = "instance"
        self.assertEqual(statistics_opened(self.instance_valid.id,type,self.user_invalid.id)[0], 400)
        self.assertEqual(statistics_opened(self.instance_invalid.id,type,self.user_valid.id)[0], 400)

    def test_statistics_opened_email_list_valid(self):
        type = "email_list"
        id = self.email_list_valid.id
        user_id = self.user_valid.id
        results = {
            "emails":2,
            "emails_this_month":2,
            "links":2,
            "links_this_month":2
        }
        test_status, test_results = statistics_opened(id,type,user_id)
        self.assertEqual(test_status, 200)
        self.assertEqual(test_results, results)

    def test_statistics_opened_email_list_invalid(self):
        type = "email_list"
        self.assertEqual(statistics_opened(self.email_list_valid.id,type,self.user_invalid.id)[0], 400)
        self.assertEqual(statistics_opened(self.email_list_invalid.id,type,self.user_valid.id)[0], 400)
    
    def test_statistics_opened_email_valid(self):
        type = "email"
        id = self.email_valid_0.email
        user_id = self.user_valid.id
        results = {
            "emails":1,
            "emails_this_month":1,
            "links":1,
            "links_this_month":1
        }
        test_status, test_results = statistics_opened(id,type,user_id)
        self.assertEqual(test_status, 200)
        self.assertEqual(test_results, results)

    def test_statistics_months_instance_valid(self):
        type = "instance"
        id = self.instance_valid.id
        user_id = self.user_valid.id
        test_status, test_results = statistics_months(id,type,user_id)
        self.assertEqual(test_status, 200)

    def test_statistics_months_instance_invalid(self):
        type = "instance"
        self.assertEqual(statistics_months(self.instance_valid.id,type,self.user_invalid.id)[0], 400)
        self.assertEqual(statistics_months(self.instance_invalid.id,type,self.user_valid.id)[0], 400)

    def test_statistics_months_email_list_valid(self):
        type = "email_list"
        id = self.email_list_valid.id
        user_id = self.user_valid.id
        test_status, test_results = statistics_months(id,type,user_id)
        self.assertEqual(test_status, 200)

    def test_statistics_months_email_list_invalid(self):
        type = "email_list"
        self.assertEqual(statistics_months(self.email_list_valid.id,type,self.user_invalid.id)[0], 400)
        self.assertEqual(statistics_months(self.email_list_invalid.id,type,self.user_valid.id)[0], 400)
    
    def test_statistics_months_email_valid(self):
        type = "email"
        id = self.email_valid_0.email
        user_id = self.user_valid.id
        test_status, test_results = statistics_months(id,type,user_id)
        self.assertEqual(test_status, 200)
