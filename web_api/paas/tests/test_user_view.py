from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from django.contrib.auth.models import User

class TestAppUserView(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create(username="user",password="pass")
        self.client.force_authenticate(user=self.user)
        self.test_client = APIClient()
        self.test_user = User.objects.create(username="updateusername",
            password="UpdatePassword123",
            email="updateemail@email.com",
            first_name="firstname",
            last_name="lastname")
        self.test_client.force_authenticate(user=self.test_user)

    def test_get_user_data(self):
        url = reverse('get-user-data')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["username"],self.user.username)

    def test_create_new_user_valid(self):
        url = reverse('create-user')
        data = {
            "username":"createusername",
            "password":"CreatePassword123",
            "email":"createemail@email.com",
            "first_name":"firstname",
            "last_name":"lastname"
        }
        response = self.client.post(url,data=data)
        self.assertEqual(response.status_code, 200)
    
    def test_create_new_user_invalid(self):
        url = reverse('create-user')
        data = {
            "username":"user",
            "password":"CreatePassword123",
            "email":"createemail@email.com",
            "first_name":"firstname",
            "last_name":"lastname"
        }
        response = self.client.post(url,data=data)
        self.assertEqual(response.status_code, 400)

    def test_update_user_valid(self):
        url = reverse('update-user')
        data = {
            "username":"updateusername",
            "password":"UpdatePassword123",
            "confirm_password":"UpdatePassword123",
            "email":"updateemail@email.com",
            "first_name":"firstnamenew",
            "last_name":"lastname"
        }
        data_username = {
            "username":"usernamevalid",
            "password":"UpdatePassword123",
            "confirm_password":"UpdatePassword123",
            "email":"updateemail@email.com",
            "first_name":"firstnamenew",
            "last_name":"lastname"
        }
        response = self.test_client.put(url,data=data)
        response_username = self.test_client.put(url,data=data_username)
        updated = User.objects.get(id=self.test_user.id)
        self.assertEqual(response.status_code,200)
        self.assertEqual(updated.first_name,"firstnamenew")
        self.assertEqual(response_username.status_code,200)
        self.assertEqual(updated.username,"usernamevalid")

    def test_update_user_invalid(self):
        url = reverse('update-user')
        data_password = {
            "username":"updateusername",
            "password":"UpdatePassword123",
            "confirm_password":"UpdatePassword1234",
            "email":"updateemail@email.com",
            "first_name":"firstnamenew",
            "last_name":"lastname"
        }
        data_validator = {
            "username":"user",
            "password":"UpdatePassword123",
            "confirm_password":"UpdatePassword123",
            "email":"updateemail@email.com",
            "first_name":"firstnamenew",
            "last_name":"lastname"
        }
        response_password = self.test_client.put(url,data=data_password)
        response_validator = self.test_client.put(url,data=data_validator)
        self.assertEqual(response_password.status_code,400)
        self.assertEqual(response_validator.status_code,400)
        