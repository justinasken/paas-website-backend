from urllib import response
from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIClient
from django.contrib.auth.models import User
from ..models import ArticleTag, ConvertedArticle, Email, EmailFormTemplateCategory, EmailList, Instance, LogAction, EmailContent, InstanceStatus

class TestAppInstanceView(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create(username="user",password="pass")
        self.client.force_authenticate(user=self.user)
        self.test_client = APIClient()
        self.test_user = User.objects.create(username="updateusername",
            password="UpdatePassword123",
            email="updateemail@email.com",
            first_name="firstname",
            last_name="lastname")
        self.test_client.force_authenticate(user=self.test_user)
        ArticleTag.objects.create(tag="testtag")
        ConvertedArticle.objects.create(url="http://www.google.com",tags="{sport}",title="title0",generated="generated0")
        EmailFormTemplateCategory.objects.create(category="test_category")
        self.sent = LogAction.objects.create(id=1,action="EMAIL SENT")
        self.opened_link = LogAction.objects.create(id=2,action="OPEN LINK")
        self.opened_email = LogAction.objects.create(id=3,action="OPEN EMAIL")
        self.idle = InstanceStatus.objects.create(id=1,status="IDLE")
        self.finished = InstanceStatus.objects.create(id=2,status="FINISHED")
        self.error = InstanceStatus.objects.create(id=3,status="ERROR")
        self.in_progress = InstanceStatus.objects.create(id=4,status="IN PROGRESS")
        self.email_list = EmailList.objects.create(name="email_list",owner=self.user,used_in=0)
        self.instance_email = EmailContent.objects.create(text="text",title="title",owner=self.user)
        self.email = Email.objects.create(email="test@test.com",email_list=self.email_list,information={})
        self.instance = Instance.objects.create(name="instance",owner=self.user,email_list=self.email_list, status=self.idle, email=self.instance_email, description="",url="http://www.google.com")

    def test_remove_multiple_instances_invalid(self):
        instance0 = Instance.objects.create(name="testinstanceinvalid0",owner=self.user,email_list=self.email_list,status=self.in_progress,email=self.instance_email,description="",url="http://www.google.com")
        instance1 = Instance.objects.create(name="testinstanceinvalid1",owner=self.user,email_list=self.email_list,status=self.in_progress,email=self.instance_email,description="",url="http://www.google.com")
        url=reverse("remove-selected-instances")
        data = {
            "ids":[instance0.id, instance1.id]
        }
        response_no_data = self.client.delete(url)
        response = self.client.delete(url,data=data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response_no_data.status_code, 400)

    def test_remove_multiple_instances_valid(self):
        instance0 = Instance.objects.create(name="testinstancevalid0",owner=self.user,email_list=self.email_list,status=self.finished,email=self.instance_email,description="",url="http://www.google.com")
        instance1 = Instance.objects.create(name="testinstancevalid1",owner=self.user,email_list=self.email_list,status=self.finished,email=self.instance_email,description="",url="http://www.google.com")
        url=reverse("remove-selected-instances")
        data = {
            "ids":[instance0.id, instance1.id]
        }
        response = self.client.delete(url,data=data)
        self.assertEqual(response.status_code, 200)

    def test_remove_instance_invalid(self):
        instance = Instance.objects.create(name="testinstanceinvalid",owner=self.user,email_list=self.email_list,status=self.in_progress,email=self.instance_email,description="",url="http://www.google.com")
        url=reverse("remove-instance")
        data = {
            "id":instance.id
        }
        response_no_data = self.client.delete(url)
        response = self.client.delete(url,data=data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response_no_data.status_code, 400)

    def test_remove_instance_valid(self):
        instance = Instance.objects.create(name="testinstanceinvalid",owner=self.user,email_list=self.email_list,status=self.finished,email=self.instance_email,description="",url="http://www.google.com")
        url=reverse("remove-instance")
        data = {
            "id":instance.id
        }
        response = self.client.delete(url,data=data)
        self.assertEqual(response.status_code, 200)

    def test_update_instance_invalid(self):
        instance = Instance.objects.create(name="testinstanceinvalid",owner=self.user,email_list=self.email_list,status=self.in_progress,email=self.instance_email,description="",url="http://www.google.com")
        url=reverse("update-instance")
        data={
            "id":instance.id,
            "name":"updatedinstancename",
            "list_id":self.email_list.id,
            "title":"title",
            "text":"text",
            "description":"description",
            "url":"http://www.google.net"
        }
        response = self.client.put(url,data=data)
        self.assertEqual(response.status_code,400)

    def test_update_instance_valid(self):
        instance = Instance.objects.create(name="testinstance",owner=self.user,email_list=self.email_list,status=self.idle,email=self.instance_email,description="",url="http://www.google.com")
        url=reverse("update-instance")
        data={
            "id":instance.id,
            "name":"updatedinstancename",
            "list_id":self.email_list.id,
            "title":"title",
            "text":"text",
            "description":"description",
            "url":"http://www.google.net"
        }
        response = self.client.put(url,data=data)
        updated = Instance.objects.get(id=instance.id)
        self.assertEqual(response.status_code,200)
        self.assertEqual(updated.name,"updatedinstancename")

    def test_create_and_send_messages(self):
        url=reverse("create-and-send-instance-emails")
        data = {
            "name":"createandsendinstance",
            "list_id":self.email_list.id,
            "title":"title",
            "text":"text",
            "description":"",
            "url":"http://www.google.com"
        }
        response = self.client.post(url,data=data)
        self.assertEqual(response.status_code,200)

    def test_send_messages(self):
        url=reverse("send-instance-emails")
        data = {
            "instance_id":self.instance.id,
            "testing":0
        }
        response=self.client.post(url,data=data)
        self.assertEqual(response.status_code, 200)

    def test_create_instance(self):
        url = reverse("create-instance")
        data={
            "name":"newinstance",
            "list_id":self.email_list.id,
            "title":"title",
            "text":"text",
            "description":"",
            "url":"http://www.google.com"
        }
        response = self.client.post(url,data=data)
        self.assertEqual(response.status_code,200)

    def test_get_instance_email(self):
        url = "%s?instance=%s"%(reverse("get-instance-email"),self.instance.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code,200)
    
    def test_get_instance(self):
        url = "%s?instance=%s"%(reverse("get-instance"),self.instance.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code,200)
        self.assertEqual(response.data["id"],self.instance.id)

    def test_get_instances(self):
        url = reverse("get-all-instances")
        response = self.client.get(url)
        self.assertEqual(response.status_code,200)

    def test_instance_log_email_invalid(self):
        url = reverse("log-email-click",kwargs={"instance":self.instance.id,"email":self.email.id,"action":self.opened_link.id})
        response_wrong_action = self.client.get(url)
        self.assertEqual(response_wrong_action.status_code,400)

    def test_instance_log_link_invalid(self):
        url = "%s?instance=%s&email=%s&action=%s" % (reverse("log-link-click"),self.instance.id,self.email.id,self.opened_email.id)
        url_fields = "%s?instance=%s&email=%s" % (reverse("log-link-click"),self.instance.id,self.email.id)
        response_wrong_action = self.client.get(url)
        response_empty_fields = self.client.get(url_fields)
        self.assertEqual(response_wrong_action.status_code,400)
        self.assertEqual(response_empty_fields.status_code,400)

    def test_instance_log_email_valid(self):
        url = reverse("log-email-click",kwargs={"instance":self.instance.id,"email":self.email.id,"action":self.opened_email.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code,200)

    def test_instance_log_link_valid(self):
        url = "%s?instance=%s&email=%s&action=%s" % (reverse("log-link-click"),self.instance.id,self.email.id,self.opened_link.id)
        response = self.client.get(url)
        self.assertEqual(response.status_code,302)
    
    def test_get_instance_statuses(self):
        url = reverse("get-all-instance-statuses")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_get_email_form_template_categories(self):
        url = reverse("get-all-email-form-template-categories")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_convert_article(self):
        url = reverse("convert-article-email")
        data = {
            "text":":ARTICLE-GENERATE ID=0: :PLAIN-RANDOM GREETING:",
            "tag":"sport"
        }
        response = self.client.post(url,data=data)
        self.assertEqual(response.status_code, 200)

    def test_get_article_tags(self):
        url = reverse("get-all-article-tags")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)