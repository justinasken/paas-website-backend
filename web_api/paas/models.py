from django.db import models
from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField


class EmailList(models.Model):
    name = models.CharField(max_length=255)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    used_in = models.IntegerField()
    def __str__(self):
        return str(self.owner.id) + "| "+self.name

    class Meta:
        verbose_name_plural = "Email lists"


class ConvertedArticle(models.Model):
    url = models.TextField(unique=True)
    tags = ArrayField(models.TextField())
    title = models.CharField(max_length=255)
    generated = models.TextField()

class Email(models.Model):
    email = models.CharField(max_length=255)
    email_list = models.ForeignKey(EmailList, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    information = models.JSONField()
    def __str__(self):
        return str(self.email_list.id) + "| "+self.email


class InstanceStatus(models.Model):
    status = models.TextField()
    def __str__(self):
        return self.status
    class Meta:
        verbose_name_plural = "Instance statuses"


class LogAction(models.Model):
    action = models.TextField()
    def __str__(self):
        return self.action
    class Meta:
        verbose_name_plural = "Log actions"

class EmailFormTemplateCategory(models.Model):
    category = models.TextField()
    def __str__(self):
        return self.category
    class meta:
        verbose_name_plural = "Email from template categories"

class EmailFormType(models.Model):
    type = models.TextField()
    has_username = models.BooleanField()
    template = models.TextField()
    title = models.CharField(max_length=255)
    category = models.ForeignKey(EmailFormTemplateCategory, on_delete=models.SET_DEFAULT, default=5)
    def __str__(self):
        return self.type
    class meta:
        verbose_name_plural = "Email from types"

class EmailContent(models.Model):
    text = models.TextField()
    title = models.CharField(max_length=255)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

class Instance(models.Model):
    name = models.CharField(max_length=255)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    email_list = models.ForeignKey(EmailList, on_delete=models.CASCADE)
    status = models.ForeignKey(
        InstanceStatus, on_delete=models.SET_DEFAULT, default=-1)
    email = models.ForeignKey(EmailContent, on_delete=models.SET_DEFAULT, default=-1)
    description = models.TextField()
    url = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return str(self.owner.id) + "| "+self.name

class InstanceLog(models.Model):
    instance = models.ForeignKey(Instance, on_delete=models.CASCADE)
    email = models.ForeignKey(Email, on_delete=models.CASCADE)
    action = models.ForeignKey(
        LogAction, on_delete=models.SET_DEFAULT, default=-1)
    created_at = models.DateTimeField(auto_now_add=True)
    class Meta:
        verbose_name_plural = "Instance logs"

class ArticleTag(models.Model):
    tag = models.TextField()
    def __str__(self):
        return self.tag
