"""
WSGI config for web_api project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.0/howto/deployment/wsgi/
"""

import os
from pathlib import Path
from django.core.wsgi import get_wsgi_application
import sys

path_home = str(Path(__file__).parents[1])
if path_home not in sys.path:
    sys.path.append(path_home)

os.environ["DJANGO_SETTINGS_MODULE"] = "web_api.settings"   
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'web_api.settings')

application = get_wsgi_application()
